<?php
class Helpers {
    public static function futureDate($month_range) {
	    $min_date = new DateTime;
	    $min_date = $min_date->format('Y-m-d H:i:s');
	    $days = 30 * (int)$month_range;
	    $max_date = date('Y-m-d', strtotime($min_date. ' + '.$days.' days'));
	    $min_epoch = strtotime($min_date);
	    $max_epoch = strtotime($max_date);
	    $rand_epoch = rand($min_epoch, $max_epoch);
	    return date('Y-m-d H:i:s', $rand_epoch);
    }
}
