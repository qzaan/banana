<?php 

return array( 
	
	/*
	|--------------------------------------------------------------------------
	| oAuth Config
	|--------------------------------------------------------------------------
	*/

	/**
	 * Storage
	 */
	'storage' => 'Session', 

	/**
	 * Consumers
	 */
	'consumers' => array(

		/**
		 * Facebook
		 */
        'Facebook' => array(
            'client_id'     => '1667611290137709',
            'client_secret' => '206bf4b526f1e10f84765678f1575921',
            'scope'         => array('email','user_likes'),
        ),
        /**
         *	Google
         */		
        'Google' => array(
		    'client_id'     => '579726928922-embc8qqd2hgcs04r0b2c4q8g9ahr80ej.apps.googleusercontent.com',
		    'client_secret' => 'oMEOMegsdCj_VKeDYn3U2WlZ',
		    'scope'         => array('userinfo_email', 'userinfo_profile'),
		),
		/**
		 * Twitter
		 */
		'Twitter' => array(
		    'client_id'     => 'Your Twitter client ID',
		    'client_secret' => 'Your Twitter Client Secret',
		    // No scope - oauth1 doesn't need scope
		),
	)

);