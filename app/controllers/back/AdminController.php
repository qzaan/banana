<?php namespace App\Controllers\Back;
use View,Input,Validator,Response,Sentry,Redirect;
class AdminController extends \BaseController {

	public function logout()
	{
		Sentry::logout();
		return View::make('back.login');
	}
	
	public function login()
	{
		return View::make('back.login');
	}

	public function registerAdmin()
	{
		return View::make('back.registerAdmin');
	}

	public function postlogin()
	{
		$validate = Validator::make(Input::all(), array(

            'email' => 'required|email',
            'password' => 'required',
		));

		if($validate->fails())
		{
			return Redirect::route('admin')->withErrors($validate)->withInput();
		}

		$credentials = array(
			'email' => Input::get('email'),
			'password' => Input::get('password'),
		);

		try
		{
			$user = Sentry::authenticate($credentials,false);
			if($user)
			{
				return View::make('back.index');
			}
			else
			{
				return Redirect::route('admin')->with('fail', 'Бүртгэлгүй хэрэглэгч');
			}
		}
		catch (\Exception $e)
		{
			return Redirect::route('admin')->with('fail', 'Бүртгэлгүй хэрэглэгч');
		}
	}

	public function postRegisterAdmin()
	{
		$validate = Validator::make(Input::all(), array(

            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required',
		));

		if($validate->fails())
		{
			return Response::json(['success'=>false,'errors'=>$validate->errors()->toArray()]);
		}

		$user = Sentry::createUser(array(
			'first_name' => Input::get('first_name'),
			'last_name' => Input::get('last_name'),
			'email' => Input::get('email'),
			'password' => Input::get('password'),
			// 'permissions' => 'sprAdmin',
			'activated' => true,
		));

		return Response::json(['success'=>true,'data'=>'Амжилттай бүртгүүллээ ;)']);
	}
}