<?php namespace App\Controllers\Back;
use App\Models\News;
use View;
class EditNewsController extends \BaseController {

	public function index()
	{
		return View::make('back.editNews', [
			'news' => News::paginate(6)
		]);
	}

	public function removeNews($id)
	{
		$news = News::find($id);
    	
    	foreach ($news->newsImage as $new) 
    	{
    		$new->delete();
    		$new->Image->delete();
    	}

    	$news->delete();
	}
}