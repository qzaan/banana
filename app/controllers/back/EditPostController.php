<?php namespace App\Controllers\Back;
use App\Models\Post;
use View;
class EditPostController extends \BaseController {

	public function index()
	{
		return View::make('back.editPost', [
			'posts' => Post::paginate(6)
		]);
	}

	public function removePost($id)
	{
		$posts = Post::find($id);
    	$posts->is_active = 0;
		$posts->save();

    	// foreach ($posts->postImage as $post) 
    	// {
    	// 	$post->delete();
    	// 	$post->Image->delete();
    	// }

    	// $posts->delete();
	}
}