<?php namespace App\Controllers\Back;
use App\Models\News;
use App\Models\Image;
use App\Models\NewsImage;
use View,Input,Validator,Response;
class RegisterNewsController extends \BaseController {

	public function index()
	{
		return View::make('back.registerNews');
	}

	public function rgstrNews()
	{
		$validate = Validator::make(Input::all(), array(

            'homeImage' => 'required',
            'title' => 'required',
			'content' => 'required',
			'sliderImages' => 'required',
		));

		if($validate->fails())
		{
			return Response::json(['success'=>false,'errors'=>$validate->errors()->toArray()]);
		}

		$news = new News();
		
		if (Input::hasFile('homeImage')) 
		{
			$picname = date("Ymdhis");
			$randomValue = rand(0, 9999999);
			$name = $randomValue . $picname;

			$news->image = $name .'.png';

			Input::file('homeImage')->move('public/images/news/home', $news->image);
		}

		$news->title = Input::get('title');
		$news->body = Input::get('content');

		$news->save();

		if (Input::hasFile('sliderImages')) 
		{
			foreach (Input::file('sliderImages') as $file) 
			{
				$picname = date("Ymdhis");
				$randomValue = rand(0, 9999999);
				$name = $randomValue . $picname;

				$image = new Image();
				$image->path = $name . '-' .'.png';
				$image->alt = 'Энэ бол (alt) хэсэг';
				$image->save();

				$newsImage = new NewsImage();
				$newsImage->news_id = $news->id;
				$newsImage->image_id = $image->id;
				$newsImage->save();

				$file->move('public/images/news/slider', $image->path);
			}
		}

		return Response::json(['success'=>true,'data'=>'Амжилттай нэмэгдлээ']);
	}
}