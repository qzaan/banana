<?php namespace App\Controllers\Back;
use App\Models\Post;
use App\Models\PostImage;
use App\Models\Image;
use App\Models\Category;
use App\Models\User;
use View,Input,Validator,Response;
class RegisterPostController extends \BaseController {

	public function index()
	{
		$categories = Category::get();
		$users = User::get();

		return View::make('back.registerPost',compact('categories','users'));
	}

	public function rgstrPost()
	{
		$validate = Validator::make(Input::all(), array(

            'category_post_id' => 'required',
            'user_post_id' => 'required',
			'title_post' => 'required',
			'content_post' => 'required',
			'homeImage_post' => 'required',
			'embed_post' => 'required',
			'rec_post' => 'required',
			'cur_post' => 'required',
			'enddate_post' => 'required',
			'sliderImages_post' => 'required',
		));

		if($validate->fails())
		{
			return Response::json(['success'=>false,'errors'=>$validate->errors()->toArray()]);
		}

		$post = new Post();
		$post->category_id = Input::get('category_post_id');
		$post->user_id = Input::get('user_post_id');
		$post->title = Input::get('title_post');
		$post->body = Input::get('content_post');
		$post->is_active = 1;

		if (Input::hasFile('homeImage_post')) 
		{
			$picname = date("Ymdhis");
			$randomValue = rand(0, 9999999);
			$name = $randomValue . $picname;

			$post->image = $name .'.png';

			Input::file('homeImage_post')->move('public/images/posts/home', $post->image);
		}

		$post->url = Input::get('embed_post');
		$post->rec = Input::get('rec_post');
		$post->cur = Input::get('cur_post');
		$post->end_date = Input::get('enddate_post');
		$post->save();

		if (Input::hasFile('sliderImages_post')) 
		{
			foreach (Input::file('sliderImages_post') as $file) 
			{
				$picname = date("Ymdhis");
				$randomValue = rand(0, 9999999);
				$name = $randomValue . $picname;

				$image = new Image();
				$image->path = $name . '-' .'.png';
				$image->alt = 'Энэ бол (alt) хэсэг';
				$image->save();

				$postImage = new PostImage();
				$postImage->post_id = $post->id;
				$postImage->image_id = $image->id;
				$postImage->save();

				$file->move('public/images/posts/slider', $image->path);
			}
		}

		return Response::json(['success'=>true,'data'=>'Амжилттай нэмэгдлээ']);

	}
}