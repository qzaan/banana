<?php namespace App\Controllers\Front;
use View;
class ContactController extends \BaseController {

	public function index()
	{
		return View::make('front.contact');
	}
}