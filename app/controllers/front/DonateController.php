<?php namespace App\Controllers\Front;
use App\Models\Post;
use View;
class DonateController extends \BaseController {

	public function index($id)
	{
		$post = Post::Find($id);
		
		return View::make('front.donate')->with('post',$post);
	}
}