<?php namespace App\Controllers\Front;
use App\Models\Post;
use App\Models\News;
use App\Models\Category;
use App\Models\Product;
use View;
class HomeController extends \BaseController {

	public function index()
	{
		$postsLast = Post::postwithcat('last');
		$postsFast = Post::postwithcat('fast');
		$categories = Category::get();
		$news = News::getLast();
		$products = Product::getLast();

		return View::make('front.index',compact('categories','postsLast','postsFast','news','products'));
	}

	public function Error404()
	{
		return View::make('front.error404');
	}
}