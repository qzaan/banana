<?php namespace App\Controllers\Front;
use Input,Lang,Session,Redirect;
class LanguageController extends \BaseController {

	public function select()
	{
		$lang = Input::get('lang');

		if($lang == 'mn')
		{
			Lang::setLocale('mn');
		}
		elseif($lang == 'en')
		{
			Lang::setLocale('en');
		}

		Session::put('lang', $lang);
        return Redirect::back();
	}

}
