<?php namespace App\Controllers\Front;
use View,Input,Sentry,OAuth,Redirect,Hash;
class LoginController extends \BaseController {

	public function getRegister()
	{
		return View::make('front.register');
	}
	
	public function getLogin()
	{
		return View::make('front.login');
	}

	public function postRegister()
	{
		try
		{
			$user = Sentry::createUser(array(
				'first_name' => Input::get('first_name'),
				'last_name' => Input::get('last_name'),
				'email' => Input::get('email'),
				'password' => Input::get('password'),
				'activated' => true,
			));
		}
		catch (Cartalyst\Sentry\Users\UserExistsException $e)
		{
			echo 'User Already Exists';
		}
	}

	public function postLogin()
	{
		$credentials = array(
				'email' => Input::get('email'),
				'password' => Input::get('password'),
			);
		try
		{
			$remember = false;
			$user = Sentry::authenticate($credentials, $remember);
			if($user)
			{
				return Redirect::to('admin');
			}
		}
		catch (\Exception $e)
		{
			return Redirect::to('login')->withErrors(array('login' => $e->getMessages()));
		}
	}

	public function logout()
	{
		Sentry::logout();
		return Redirect::to('/');
	}

	public function loginWithGoogle() {

        // get data from input
        $code = Input::get( 'code' );

        // get google service
        $googleService = OAuth::consumer( 'Google' );

        // check if code is valid

        // if code is provided get user data and sign in
        if ( !empty( $code ) ) {

            // This was a callback request from google, get the token
            $token = $googleService->requestAccessToken( $code );

            // Send a request with it
            $result = json_decode( $googleService->request( 'https://www.googleapis.com/oauth2/v1/userinfo' ), true );
            var_dump($result);
            if(!empty($token)){

                try{
                    // Find the user using the user id
                    $user = Sentry::findUserByLogin($result['email']);

                    // Log the user in
                    Sentry::login($user, false);

                    return Redirect::route('home');
                }
                catch (\Cartalyst\Sentry\Users\UserNotFoundException $e)
                {
                    // Register the user
                    $user = Sentry::register(array(
                        'activated' =>  1,
                        'email'      => $result['email'],
                        'password'   => Hash::make(uniqid(time())),
                        'first_name' => $result['name']
                    ));

                    //$usergroup = Sentry::getGroupProvider()->findById(2);
                    //$user->addGroup($usergroup);

                    Sentry::login($user, false);

                    return Redirect::route('home');
                }

            }

        }
        // if not ask for permission first
        else {
            // get googleService authorization
            $url = $googleService->getAuthorizationUri();

            // return to facebook login url
            return Redirect::to( (string)$url );
        }
    }

    public function loginWithFacebook() {
	    	// get data from input
	    $code = Input::get( 'code' );

	    // get fb service
	    $fb = OAuth::consumer( 'Facebook' );

	    // check if code is valid

	    // if code is provided get user data and sign in
	    if ( !empty( $code ) ) {

	        // This was a callback request from facebook, get the token
	        $token = $fb->requestAccessToken( $code );

	        // Send a request with it
	        $result = json_decode( $fb->request( '/me' ), true );

	        var_dump($result);
	    }
	    // if not ask for permission first
	    else {
	        // get fb authorization
	        $url = $fb->getAuthorizationUri();

	        // return to facebook login url
	         return Redirect::to( (string)$url );
	    }
    }

    public function loginWithTwitter()
    {
    	
    }
}