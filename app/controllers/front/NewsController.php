<?php namespace App\Controllers\Front;
use App\Models\News;
use App\Models\Post;
use View;
class NewsController extends \BaseController {

	public function index()
	{
		return View::make('front.news', [
			'news' => News::paginate(6)
		]);
	}
	public function show($id)
	{
		$news = News::Find($id);
		$newsSimilar = News::getSimilarNews($id);
		$posts = Post::lastTwoPost();

		return View::make('front.newsdetail',compact('news','posts','newsSimilar'));
	}
}
