<?php namespace App\Controllers\Front;
use App\Models\Post;
use App\Models\Category;
use App\Models\Comment;
use View,Input,Validator,Response;
class PostController extends \BaseController {

	public function index()
	{
		$posts = Post::paginate(4);
		$categories = Category::get();

		return View::make('front.posts',compact('posts','categories'));
	}
	public function show($id)
	{
		$post = Post::Find($id);
		$comments = Comment::getComments($id);
		$comment_count = Comment::getCommentCount($id);

		return View::make('front.postdetail',compact('post','images','comments','comment_count'));
	}

	public function showmore($id)
	{
		$comments = Comment::getComments($id);

		return View::make('front.commentPagination')->with('comments',$comments)->render();
	}

	public function searchAny()
	{
		return Input::get('searchValue');
	}

	public function sendPcomment()
	{
	    $validate = Validator::make(Input::all(), array(

            'cMail' => 'required|email',
            'cTitle' => 'required|max:255|alpha_dash',
			'cBody' => 'required|max:900|alpha_dash',
		));

		if($validate->fails())
		{
			return Response::json(['success'=>false,'errors'=>$validate->errors()->toArray()]);
		}

		// save comment
		$postID = Input::get('_postID');
		if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
		    $ip = $_SERVER['HTTP_CLIENT_IP'];
		} elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
		    $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
		} else {
		    $ip = $_SERVER['REMOTE_ADDR'];
		}

		$comment = new Comment();
		$comment->post_id = $postID;
		$comment->title = Input::get('cTitle');
		$comment->body = Input::get('cBody');
		$comment->email = Input::get('cMail');
		$comment->ip = $ip;
		$comment->save();

		// refresh comments
		$comments = Comment::getComments($postID);
		$comment_count = Comment::getCommentCount($postID);

		return Response::json([
			'success' => true,
			'body' => View::make('front.commentNew')->with('comments',$comments)->with('comment_count',$comment_count)->render(),
			'commentCounter' => View::make('front.commentCount')->with('comment_count',$comment_count)->render()
		]);
	}
}
