<?php namespace App\Controllers\Front;
use App\Models\Request;
use View,Input,Validator,Response;
class RqstDonationController extends \BaseController {

	public function index()
	{
		return View::make('front.rqstdonate');
	}

	public function rqstDonate()
	{
		$validate = Validator::make(Input::all(), array(

            'rqstPhone' => 'required',
            'rqstMail' => 'email',
			'rqstURL' => 'required',
			'rqstTitle' => 'required',
			'rqstBody' => 'required',
		));

		if($validate->fails())
		{
			return Response::json(['success'=>false,'errors'=>$validate->errors()->toArray()]);
		}

		$request = new Request();
		$request->user_id = Input::get('_userID');
		$request->title = Input::get('rqstTitle');
		$request->email = Input::get('rqstMail');
		$request->phone_number = Input::get('rqstPhone');
		$request->body = Input::get('rqstBody');
		$request->url = Input::get('rqstURL');

		if (Input::hasFile('rqstImage')) 
		{
			$pic = Input::file('rqstImage');
			$picname = $pic->getClientOriginalName();
			$subName = mb_substr($picname,0,5);
			$randomValue = rand(0, 9999999);
			$name = $randomValue . $subName;

			$request->image = $name .'.png';

			Input::file('rqstImage')->move('public/images/user/', $request->image);
		}
		else
		{
			$request->image = 'noImage.png';
		}

		$request->save();

		return Response::json(['success'=>true,'data'=>$request->image,'value'=>$picname]);
	}
}