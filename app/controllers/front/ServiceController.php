<?php namespace App\Controllers\Front;
use View;
class ServiceController extends \BaseController {

	public function index()
	{
		return View::make('front.service');
	}
}