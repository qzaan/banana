<?php namespace App\Controllers\Front;
use App\Models\Product;
use App\Models\ProductCategory;
use View,Input,Cache,Paginator;
class ShopController extends \BaseController {

	public function index()
	{
		$products = Product::getAll();
		$categories = ProductCategory::getAll();

		return View::make('front.shop',compact('products','categories'));
	}

	public function show($id)
	{
		$product = Product::Find($id);
		$similarProducts = Product::getSimilar( $product->category_id );

		return View::make('front.shopdetail',compact('product','similarProducts'));
	}

	public function showCartEmpty()
	{
		return View::make('front.productCartEmpty')->render();
	}

	public function getAllCache()
	{
		$arr_cache_all = array();
		$arr_cache = Cache::get('allKeys');
		$total = 0;

		for ($i = 0; $i < sizeof($arr_cache); $i++) 
		{ 
			$arr = Cache::get($arr_cache[$i]);
			array_push( $arr_cache_all, $arr );
			
			$total += ( Cache::get($arr_cache[$i])['price'] * Cache::get($arr_cache[$i])['count']);
		}
		$page = null;
		$perPage = 4;
		$page = $page ? (int) $page * 1 : (isset($_REQUEST['page']) ? (int) $_REQUEST['page'] * 1 : 1);
		$offset = ($page * $perPage) - $perPage;
	 	$paginateData =  Paginator::make(array_slice($arr_cache_all, $offset, $perPage, true), count($arr_cache_all), $perPage);

		return View::make('front.productCart')->with('arr_cache_all',$paginateData)->with('total',$total)->render();
	}

	public function clearAllCache()
	{
		Cache::flush();
	}

	public function pushProduct()
	{	
		$array = array('id'=>$_POST['id'], 'title'=>$_POST['title'], 'price'=>$_POST['price'],'size'=>$_POST['size'],'url'=>$_POST['url'],'count'=>$_POST['count']);
		$key = 'pr'.$_POST['id'].$_POST['size'];
		$time = 60*24*365;

		// All keys
		if (Cache::has('allKeys'))
		{
			if(!Cache::has($key))
			{
				$arr_keys = Cache::get('allKeys');
				array_push( $arr_keys,$key );
				Cache::put('allKeys', $arr_keys, $time);
			}
		}
		else
		{
			$arr_keys = array($key);
			Cache::put('allKeys', $arr_keys, $time);
		}

		// Push Product
		if (Cache::has($key))
		{
			$array['count'] = $array['count'] + Cache::get($key)['count'];
			Cache::put($key, $array, $time);
		}
		else
		{
			Cache::put($key, $array, $time);
		}

		return $this->getAllCache();
	}

	public function popProduct($id,$size)
	{
		$time = 60*24*365;
		$key = 'pr'.$id.$size;
		Cache::forget($key);

		$arr_cache_all = Cache::get('allKeys');
		$new_arr=array();
		foreach($arr_cache_all as $value)
		{
		    if($value==$key)
		    {
		        continue;
		    }
		    else
		    {
		        $new_arr[]=$value;
		    }     
		}
		Cache::put('allKeys', $new_arr, $time);

		return $this->getAllCache();
	}
}