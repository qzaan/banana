<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('posts', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('category_id')->unsigned();
			$table->integer('user_id')->unsigned();
			$table->string('title');
			$table->text('body');
			$table->boolean('is_active');
			$table->string('image');
			$table->string('url');
			$table->double('rec');
			$table->double('cur');
			$table->date('end_date');
			$table->timestamps();
		});
		Schema::table('posts', function(Blueprint $table)
		{
    		$table->foreign('category_id')->references('id')->on('categories')->onDelete('no action');
    		$table->foreign('user_id')->references('id')->on('users')->onDelete('no action');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('posts');
	}

}
