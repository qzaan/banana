<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArchiveTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('archive', function(Blueprint $table)	
		{
			$table->increments('id');
			$table->integer('user_id')->unsigned();
			$table->integer('product_id')->unsigned();
			$table->integer('total');
			$table->timestamps();
		});
		Schema::table('archive', function(Blueprint $table)
		{
			$table->foreign('product_id')->references('id')->on('products')->onDelete('no action');
			$table->foreign('user_id')->references('id')->on('users')->onDelete('no action');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('archive');
	}

}
