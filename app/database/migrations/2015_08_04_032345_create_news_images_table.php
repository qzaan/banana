<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewsImagesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('news_images', function(Blueprint $table)	
		{
			$table->increments('id');
			$table->integer('news_id')->unsigned();
			$table->integer('image_id')->unsigned();
			$table->timestamps();
		});
		Schema::table('news_images', function(Blueprint $table)
		{
			$table->foreign('news_id')->references('id')->on('news')->onDelete('no action');
			$table->foreign('image_id')->references('id')->on('images')->onDelete('no action');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schema::drop('news_images');
	}

}
