<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostImagesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('post_images', function(Blueprint $table)	
		{
			$table->increments('id');
			$table->integer('post_id')->unsigned();
			$table->integer('image_id')->unsigned();
			$table->timestamps();
		});
		Schema::table('post_images', function(Blueprint $table)
		{
			$table->foreign('post_id')->references('id')->on('posts')->onDelete('no action');
			$table->foreign('image_id')->references('id')->on('images')->onDelete('no action');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schema::drop('post_images');
	}

}
