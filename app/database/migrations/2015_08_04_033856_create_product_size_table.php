<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductSizeTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('product_size', function(Blueprint $table)	
		{
			$table->increments('id');
			$table->integer('product_id')->unsigned();
			$table->string('size');
			$table->integer('quantity');
			$table->timestamps();
		});

		Schema::table('product_size', function(Blueprint $table)
		{
			$table->foreign('product_id')->references('id')->on('products')->onDelete('no action');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schema::drop('product_size');
	}

}
