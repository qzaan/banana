<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostArchiveTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('post_archive', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('user_id')->unsigned();
			$table->integer('post_id')->unsigned();
			$table->double('total');
			$table->timestamps();
		});

		Schema::table('post_archive', function(Blueprint $table)
		{
			$table->foreign('user_id')->references('id')->on('users')->onDelete('no action');
			$table->foreign('post_id')->references('id')->on('posts')->onDelete('no action');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('post_archive');
	}

}
