
<?php

class CategoryTableSeeder extends Seeder{
	public function run()
	{
		DB::table('categories')->delete();

				$cat = array(
						array(
										'title' => 'Хүүхэд',
										'created_at' => new DateTime,
                		'updated_at' => new DateTime
			),
						array(
										'title' => 'Хавдар',
										'created_at' => new DateTime,
                		'updated_at' => new DateTime
			)
		);

		DB::table('categories')->insert($cat);

	}
}
