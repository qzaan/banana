<?php

use App\Models\Image;
use App\Models\News;

class NewsSeeder extends Seeder{
	public function run()
	{
		DB::table('news')->delete();

		$faker = Faker\Factory::create();
		 
		for ($i = 0; $i < 20; $i++)
		{
		  $news = News::create (array(
		    'title' => $faker->word,
		    'body' => $faker->text,
		    'image' => $faker->imageUrl($width = 816, $height = 461),
		    'updated_at' => new DateTime,
		    'created_at' => new DateTime
		  ));
		  $random_temp = rand(3, 8);
		  for ($j = 0; $j <= $random_temp; $j++)
		  {
		  	$image = Image::create( array(
				'alt' => $faker->word,
				'path' => $faker->imageUrl($width = 816, $height = 461),
				'updated_at' => new DateTime,
				'created_at' => new DateTime
			));
			$news_image = array('news_id' => $news->id, 'image_id' => $image->id);
			DB::table('news_images')->insert($news_image);
		  }
		}	
	}
}
