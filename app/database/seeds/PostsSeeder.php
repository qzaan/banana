<?php

use App\Models\Image;
use App\Models\Post;
use App\Models\Category;
use App\Models\User;
use App\Models\Comment;

class PostsSeeder extends Seeder{
	public function run()
	{
		$faker = Faker\Factory::create();

		for ($i = 0; $i < 5; $i++)
		{
		  $category = Category::create (array(
		    'title' => $faker->word,
		    'updated_at' => new DateTime,
		    'created_at' => new DateTime
		  ));
		  for ($j = 0; $j <= 5; $j++)
		  {
			$user = User::create( array(
				  'email' => $faker->email,
				  'first_name' => $faker->firstName,
				  'last_name' => $faker->lastName,
				  'updated_at' => new DateTime,
				  'created_at' => new DateTime
			  ));

	              	$rec = $faker->numberBetween($min = 1000000, $max = 80000000);
			$cur = rand(0, 100) * $rec / 100;
		  	$post = Post::create( array(
				'title' => $faker->sentence,
				'body' => $faker->text,
				'url' => 'https://www.youtube.com/embed/eFiE0BJxTWE',
				'rec' => $rec,
				'cur' => $cur,
				'end_date' => Helpers::futureDate('6'),
				'image' => $faker->imageUrl($width = 535, $height = 406),
				'updated_at' => new DateTime,
				'created_at' => new DateTime,
				'user_id' => $user->id,
				'category_id' => $category->id
			));

		  	$random_temp = rand(3, 8);
			for ($a = 0; $a <= $random_temp; $a++)
			  {
			  	$image = Image::create( array(
					'alt' => $faker->sentence,
					'path' => $faker->imageUrl($width = 810, $height = 458),
					'updated_at' => new DateTime,
					'created_at' => new DateTime
				));
				$post_image = array('post_id' => $post->id, 'image_id' => $image->id);
				DB::table('post_images')->insert($post_image);
			  }

			$temp_count = rand(3, 20);
			for($k = 0; $k < $temp_count; $k++)
			{
				Comment::create( array(
					'title' => $faker->sentence,
					'body' => $faker->text(200),
					'email' => $faker->email,
					'ip' => $faker->ipv4,
					'post_id' => $post->id,
					'updated_at' => new DateTime,
					'created_at' => new DateTime
				));

			}
		  }
		}
	}
}
