<?php

use App\Models\Image;
use App\Models\Product;
use App\Models\ProductSize;
use App\Models\ProductCategory;


class ProductsSeeder extends Seeder{
	public function run()
	{
		$faker = Faker\Factory::create();
		 
		for ($k = 0; $k < 5; $k++)
		{
			$pcategory = ProductCategory::create(array(
				'title' => $faker->word,
			    	'updated_at' => new DateTime,
			    	'created_at' => new DateTime
			));
		for ($i = 0; $i < 5; $i++)
		{
		  $product = Product::create (array(
		    'title' => $faker->sentence,
		    'description' => $faker->text(200),
		    'price' => rand(5000, 100000),
		    'image' => $faker->imageUrl($width = 318, $height = 320),
		    'category_id' => $pcategory->id,
		    'updated_at' => new DateTime,
		    'created_at' => new DateTime
		  ));
		  $random_temp = rand(2, 5);
		  for ($j = 0; $j <= $random_temp; $j++)
		  {
		  	$image = Image::create( array(
				'alt' => $faker->word,
				'path' => $faker->imageUrl($width = 342, $height = 492),
				'updated_at' => new DateTime,
				'created_at' => new DateTime
			));
			$product_image = array('product_id' => $product->id, 'image_id' => $image->id);
			DB::table('product_images')->insert($product_image);
		  }	
		  $random_temp = rand(1, 5);
		  for ($j = 0; $j <= $random_temp; $j++)
		  {
		  	$product_size = ProductSize::create( array(
				'size' => $faker->word(3),
				'quantity' => rand(0, 20),
				'product_id' => $product->id,
				'updated_at' => new DateTime,
				'created_at' => new DateTime
			));
		  }
		}	
		}
	}
}
