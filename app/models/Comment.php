<?php namespace App\Models;
use DB;
class Comment extends \Eloquent {

	protected $fillable = [];
	protected $table = 'comments';

	public static function getComments($postId)
	{
		$res = DB::table('comments')
			->select('id', 'title', 'body', 'email', 'ip', 'created_at')
			->orderBy('created_at', 'DESC')
			->where('post_id',$postId)
			->paginate(5);

        return $res;
	}

	public static function getCommentCount($postId)
	{
        $res = DB::table('comments')
        	->groupBy('post_id')
        	->having('post_id', '=', $postId)
        	->count();

        return $res;
	}
}
