<?php namespace App\Models;
use DB;
class News extends \Eloquent {

	protected $fillable = [];
	protected $table = 'news';

	public function newsImage()
	{
		return $this->hasMany('App\Models\NewsImage','news_id');
	}

	public static function getSimilarNews($id)
	{
		$res = DB::table('news')
			->select('id', 'title', 'body', 'image', 'created_at')
	        ->orderBy('created_at', 'desc')
	        ->where('news.id','!=','$id')
	        ->take(4)
	        ->get();

		return $res;
	}

	public static function getLast()
	{
		$res = DB::table('news')
			->select('id', 'title', 'body', 'image','created_at')
	        ->orderBy('created_at', 'desc')
	        ->take(3)
	        ->get();

        return $res;
	}
}