<?php namespace App\Models;

class NewsImage extends \Eloquent {
	
	protected $fillable = [];
	protected $table = 'news_images';

	public function Image()
	{
		return $this->belongsTo('App\Models\Image','image_id');
	}

}