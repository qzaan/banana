<?php namespace App\Models;
use DB;
class Post extends \Eloquent {

	protected $fillable = [];
	protected $table = 'posts';

	public function user()
	{
		return $this->belongsTo('App\Models\User','user_id');
	}

	public function postArchive()
	{
		return $this->belongsTo('App\Models\PostArchive','post_id');
	}

	public function postImage()
	{
		return $this->hasMany('App\Models\PostImage');	
	}

	public function category()
	{
		return $this->belongsTo('App\Models\Category','category_id');
	}	

	public function comment()
	{
		return $this->hasMany('App\Models\Comment');
	}

	public static function postwithcat($type)
	{
		if($type == 'last')
		{
			$res = DB::table('posts')
				->select('posts.id', 'categories.title as category', 'posts.image', 'posts.title', 'posts.rec', 'posts.cur', 'posts.body')
	            ->join('categories', 'posts.category_id', '=', 'categories.id')
	            ->orderBy('posts.created_at', 'asc')
	            ->take(6)
	            ->get();
		}
		else if($type == 'fast')
		{
			$res = DB::table('posts')
				->select('posts.id', 'categories.title as category', 'posts.image', 'posts.title', 'posts.rec', 'posts.cur', 'posts.body')
	            ->join('categories', 'posts.category_id', '=', 'categories.id')
	            ->orderBy('posts.end_date', 'asc')
	            ->take(2)
	            ->get();
		}
		
		return $res;
	}

	public static function lastTwoPost()
	{
		$res = DB::table('posts')
			->select('posts.id', 'categories.title as category', 'posts.image', 'posts.title')
	        ->join('categories', 'posts.category_id', '=', 'categories.id')
			->orderBy('posts.end_date', 'asc')
            ->take(2)
            ->get();

        return $res;
	}
}