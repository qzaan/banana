<?php namespace App\Models;

class PostImage extends \Eloquent {
	
	protected $fillable = [];
	protected $table = 'post_images';

	public function Image()
	{
		return $this->belongsTo('App\Models\Image','image_id');
	}
}