<?php namespace App\Models;
use DB;
class Product extends \Eloquent {

	protected $fillable = [];
	protected $table = 'products';

	public function productSize()
	{
		return $this->hasMany('App\Models\ProductSize');
	}

	public function productCategory()
	{
		return $this->belongsTo('App\Models\ProductCategory','category_id');
	}

	public function productImages()
	{
		return $this->hasMany('App\Models\ProductImage');
	}

	public static function getSimilar($id)
	{
		$res = DB::table('products')
			->select('id', 'title', 'description', 'price', 'image')
	        ->orderBy('created_at', 'desc')
	        ->where('products.category_id',$id)
	        ->take(3)
	        ->get();

		return $res;
	}

	public static function getLast()
	{
		$res = DB::table('products')
				->select('product_size.size as size','products.id', 'products.title', 'products.description', 'products.price', 'products.image')
				->join('product_size', 'products.id', '=', 'product_size.product_id')
	            ->orderBy('products.created_at', 'desc')
	            ->take(5)
	            ->get();

	    return $res;
	}

	public static function getAll()
	{
		$res = DB::table('products')
	            ->join('product_categories', 'products.category_id', '=', 'product_categories.id')
	            ->join('product_size', 'products.id', '=', 'product_size.product_id')
	            ->select('product_size.size as size','product_categories.title as category','products.id', 'products.title', 'products.description', 'products.price', 'products.image')
	            ->orderBy('products.created_at', 'desc')
	            ->paginate(4);

	    return $res;
	}
}
