<?php namespace App\Models;
use DB;
class ProductCategory extends \Eloquent {
	protected $fillable = [];
	protected $table = 'product_categories';

	public static function getAll()
	{
		$res = DB::table('product_categories')
	            ->select('product_categories.title as categoryName')
	            ->get();
	            
	    return $res;
	}
}
