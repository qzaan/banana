<?php namespace App\Models;

class ProductImage extends \Eloquent {
	
	protected $fillable = [];
	protected $table = 'product_images';

	public  function image()
	{
		return $this->belongsTo('App\Models\Image','image_id');
	}
}
