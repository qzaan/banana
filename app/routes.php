<?php

Route::get('/',array('uses' =>'App\Controllers\Front\HomeController@index','as'=>'home'));

// Request donation
// Route::group(['before' => 'auth|standardUser|admin'], function(){
// Route::group(array('before' => 'auth'), function(){

	Route::get('/r','App\Controllers\Front\RqstDonationController@index');
	Route::post('/rqstDonate','App\Controllers\Front\RqstDonationController@rqstDonate');
// });

// Post
Route::get('/p','App\Controllers\Front\PostController@index');
Route::get('/p/{id}','App\Controllers\Front\PostController@show');
Route::get('/showmore/{id}', 'App\Controllers\Front\PostController@showmore');		// Comment showmore
Route::post( '/sendPcomment', 'App\Controllers\Front\PostController@sendPcomment');	// Comment

// News
Route::get('/n', 'App\Controllers\Front\NewsController@index');
Route::get('/n/{id}', 'App\Controllers\Front\NewsController@show');

// Contact
Route::get('/c', 'App\Controllers\Front\ContactController@index');

// Shop
Route::get('/s', 'App\Controllers\Front\ShopController@index');
Route::get('/s/{id}', 'App\Controllers\Front\ShopController@show');
Route::post( '/pushProduct', 'App\Controllers\Front\ShopController@pushProduct');
Route::post( '/popProduct/{id}/{size}', 'App\Controllers\Front\ShopController@popProduct');
Route::post( '/showCartProduct', 'App\Controllers\Front\ShopController@getAllCache');
Route::post( '/showCartEmpty', 'App\Controllers\Front\ShopController@showCartEmpty');
Route::post( '/clearAllCache', 'App\Controllers\Front\ShopController@clearAllCache');

// Donate
Route::get('/d/{id}', 'App\Controllers\Front\DonateController@index');

// Terms Of Service
Route::get('/tos', 'App\Controllers\Front\ServiceController@index');

// Search
Route::post( '/searchAny', 'App\Controllers\Front\PostController@searchAny');

// Language
Route::get('/language', 
   array(
          'as' => 'language', 
          'uses' => 'App\Controllers\Front\LanguageController@select'
         )
);

// Login
Route::get('register', 'App\Controllers\Front\LoginController@getRegister');
Route::get('login', 'App\Controllers\Front\LoginController@getLogin');

Route::post('login', 'App\Controllers\Front\LoginController@postLogin');
Route::post('register', 'App\Controllers\Front\LoginController@postRegister');
Route::get('logout', 'App\Controllers\Front\LoginController@logout');

// Route::group(array('before' => 'auth'), function(){

	Route::get('/admin', array('uses' =>'App\Controllers\Back\AdminController@login', 'as' => 'admin') );
	Route::post( '/admin/controll', array('uses' => 'App\Controllers\Back\AdminController@postlogin', 'as' => 'postlogin') );
	Route::get('/Out', array('uses' =>'App\Controllers\Back\AdminController@logout', 'as' => 'adminOut') );

	Route::get('registerAdmin', 'App\Controllers\Back\AdminController@registerAdmin');
	Route::post('postRegisterAdmin', 'App\Controllers\Back\AdminController@postRegisterAdmin');
// });

Route::get('google', array('as' => 'google', 'uses' => 'App\Controllers\Front\LoginController@loginWithGoogle'));
Route::get('facebook', array('as' => 'facebook', 'uses' => 'App\Controllers\Front\LoginController@loginWithFacebook'));
Route::get('twitter', array('as' => 'twitter', 'uses' => 'App\Controllers\Front\LoginController@loginWithTwitter'));

// Admin Controll (Backend)
Route::get('/registerNews', array('uses' =>'App\Controllers\Back\RegisterNewsController@index', 'as' => 'registerNews') );
Route::post('/rgstrNews','App\Controllers\Back\RegisterNewsController@rgstrNews');
Route::get('/editNews', array('uses' =>'App\Controllers\Back\EditNewsController@index', 'as' => 'editNews') );
Route::post('/removeNews/{id}','App\Controllers\Back\EditNewsController@removeNews');

Route::get('/registerPost', array('uses' =>'App\Controllers\Back\RegisterPostController@index', 'as' => 'registerPost') );
Route::post('/rgstrPost','App\Controllers\Back\RegisterPostController@rgstrPost');
Route::get('/editPost', array('uses' =>'App\Controllers\Back\EditPostController@index', 'as' => 'editPost') );
Route::post('/removePost/{id}','App\Controllers\Back\EditPostController@removePost');

// Error Page
Route::get('{slug}','App\Controllers\Front\HomeController@Error404');