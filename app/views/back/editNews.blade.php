@extends('back._layout.layout')
@section('content')
    
    <br>
    <h4>Мэдээ засварлах</h4>
    <hr>
    
    <table class="table table-bordered table-hover" id="table">
        <thead>
            <tr>
                <th>Зураг</th>
                <th>Гарчиг</th>
                <th>Агуулга</th>
                <th>Нийтэлсэн</th>
                <th>Засах</th>
            </tr>
        </thead>
        <tbody  id="booklistref">
            @foreach($news as $new)
            <tr>
                <td><img height="60" width="80" src="/images/news/home/{{$new->image}}"/></td>
                <td>
                	<?php
                        echo (strlen($new->title) >= 34) ? mb_substr($new->title,0,34).'...' : $new->title;
                    ?>
                </td>
                <td>
                	<?php
                        echo (strlen($new->body) >= 60) ? mb_substr($new->body,0,60).'...' : $new->body;
                    ?>
                </td>
                <td>{{$new->created_at}}</td>
                <td>
                	<button class="btn btn-xs btn-primary" onclick="editNews({{$new->id}})" >
                		<i class="fa fa-edit"></i>
                	</button>
                	<button class="btn btn-xs btn-danger" onclick="removeNews({{$new->id}})" style="float:right">
                		<i class="fa fa-times"></i>
                	</button>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
    <br>
    <?php echo $news->links(); ?>

@stop