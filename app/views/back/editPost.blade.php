@extends('back._layout.layout')
@section('content')

    <br>
    <h4>Хандив засварлах</h4>
    <hr>

    <table class="table table-bordered table-hover" id="table">
        <thead>
            <tr>
                <th>Зураг</th>
                <th>Категори</th>
                <th>Гарчиг</th>
                <th>Нийт</th>
                <th>Одоо байгаа</th>
                <th>Дуусах хугацаа</th>
                <th>Хандивыг хүсэмжлэгч</th>
                <th>Засах</th>
            </tr>
        </thead>
        <tbody  id="booklistref">
            @foreach($posts as $post)
            <tr>
                <td><img height="60" width="80" src="/images/posts/home/{{$post->image}}"/></td>
                <td>{{$post->category['title']}}</td>
                <td>
                	<?php
                        echo (strlen($post->title) >= 24) ? mb_substr($post->title,0,24).'...' : $post->title;
                    ?>
                </td>
                <td>{{$post->rec}}</td>
                <td>{{$post->cur}}</td>
                <td>{{$post->created_at}}</td>
                <td>{{$post->user['email']}}</td>
                <td>
                	<button class="btn btn-xs btn-primary" onclick="editPost({{$post->id}})" >
                		<i class="fa fa-edit"></i>
                	</button>
                	<button class="btn btn-xs btn-danger" onclick="removePost({{$post->id}})" style="float:right">
                		<i class="fa fa-times"></i>
                	</button>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
    <br>
    <?php echo $posts->links(); ?>

@stop