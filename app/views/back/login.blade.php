<!DOCTYPE html>
<html >
  <head>
    <meta charset="UTF-8">
    <title>Нэвтрэх</title>
    <style>
    	@import url(http://fonts.googleapis.com/css?family=Exo:100,200,400);
		@import url(http://fonts.googleapis.com/css?family=Source+Sans+Pro:700,400,300);

		body{
			margin: 0;
			padding: 0;
			background: #fff;

			color: #fff;
			font-family: Arial;
			font-size: 12px;
			overflow: hidden;
		}

		.body{
			position: absolute;
			top: 0px;
			left: -20px;
			right: -40px;
			bottom: -40px;
			width: auto;
			height: auto;
			background-image: url('images/back/login_bg.jpg');
			background-size: cover;
			-webkit-filter: blur(1px);
			z-index: 0;
		}

		.grad{
			position: absolute;
			top: -20px;
			left: -20px;
			right: -40px;
			bottom: -40px;
			width: auto;
			height: auto;
			background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(0,0,0,0)), color-stop(100%,rgba(0,0,0,0.65))); /* Chrome,Safari4+ */
			z-index: 1;
			opacity: 0.7;
		}

		.header{
			position: absolute;
			top: calc(50% - 66px);
			left: calc(50% - 255px);
			z-index: 2;
		}

		.header div{
			float: left;
			color: #fff;
			font-family: 'Exo', sans-serif;
			font-size: 35px;
			font-weight: 200;
		}

		.header div span{
			color: #00acc1 !important;
		}

		.login{
			position: absolute;
			top: calc(50% - 75px);
			left: calc(50% - 50px);
			height: 150px;
			width: 350px;
			padding: 10px;
			z-index: 2;
		}

		.login input[type=text]{
			width: 250px;
			height: 30px;
			background: transparent;
			border: 1px solid rgba(255,255,255,0.6);
			border-radius: 2px;
			color: #fff;
			font-family: 'Exo', sans-serif;
			font-size: 16px;
			font-weight: 400;
			padding: 4px;
		}

		.login input[type=password]{
			width: 250px;
			height: 30px;
			background: transparent;
			border: 1px solid rgba(255,255,255,0.6);
			border-radius: 2px;
			color: #fff;
			font-family: 'Exo', sans-serif;
			font-size: 16px;
			font-weight: 400;
			padding: 4px;
			margin-top: 10px;
		}

		.login input[type=submit]{
			width: 260px;
			height: 35px;
			background: #fff;
			border: 1px solid #fff;
			cursor: pointer;
			border-radius: 2px;
			color: #4b4b4b;
			font-family: 'Exo', sans-serif;
			font-size: 16px;
			font-weight: 400;
			padding: 6px;
			margin-top: 10px;
		}

		.login input[type=submit]:hover{
			opacity: 0.8;
		}

		.login input[type=submit]:active{
			opacity: 0.6;
		}

		.login input[type=text]:focus{
			outline: none;
			border: 1px solid rgba(255,255,255,0.9);
		}

		.login input[type=password]:focus{
			outline: none;
			border: 1px solid rgba(255,255,255,0.9);
		}

		.login input[type=submit]:focus{
			outline: none;
		}

		::-webkit-input-placeholder{
		   color: rgba(255,255,255,0.6);
		}

		::-moz-input-placeholder{
		   color: rgba(255,255,255,0.6);
		}

		.alert{
			margin-top: 10px;
		}
    </style>
    
  </head>

  <body>

    <div class="body"></div>
		<div class="grad"></div>
		<div class="header">
			<div>Админ<span><br>Удирдлага</span></div>
		</div>
		<br>
		<div class="login">
			{{Form::open( array('method' => 'post','route' => 'postlogin') )}}

				<input name="email" type="text" placeholder="{{Lang::get('category.loginEmail')}}"><br>
				<input name="password" type="password" placeholder="{{Lang::get('category.loginPassword')}}"><br>
				<input id="adminLogin" type="submit" value="{{Lang::get('category.login')}}">

			{{Form::token()}}
            {{Form::close()}}
            
  			@if(Session::has('fail'))
  				<div class="alert alert-danger">{{ Session::get('fail') }}</div>
  			@endif

            @if ($errors->has('email'))
				<div class="alert alert-warning">{{ $errors->first('email') }}</div>
			@elseif ($errors->has('password'))
				<div class="alert alert-warning">{{ $errors->first('password') }}</div>
			@endif
		</div>
    
  </body>

  <script src="/js/jquery-2.1.1.min.js"></script>
  <script src="/js/main_back.js"></script>
</html>
