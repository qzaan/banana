{{Form::open( array('method' => 'post','id'=>'formRegisterAdmin','enctype' => 'multipart/form-data') )}}

	<input name="first_name" type="text" placeholder="{{Lang::get('category.first_name')}}"><br>
	<input name="last_name" type="text" placeholder="{{Lang::get('category.last_name')}}"><br>
	<input name="email" type="text" placeholder="{{Lang::get('category.loginEmail')}}"><br>
	<input name="password" type="password" placeholder="{{Lang::get('category.loginPassword')}}"><br>
	<input id="registerAdmin" type="button" value="{{Lang::get('category.register')}}">

	<div id="error"></div>
	<div id="success"></div>

	<script src="/js/jquery-2.1.1.min.js"></script>
  	<script src="/js/main_back.js"></script>
{{Form::token()}}
{{Form::close()}}