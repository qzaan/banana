@extends('back._layout.layout')
@section('content')

	<br>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-folder-open-o"></i> Мэдээ оруулах
                </div>
                <div class="panel-body">
                    <div class="row">

                        <div class="col-lg-12">

                        	{{Form::open( array('method' => 'post','id'=>'rgstrNews','enctype' => 'multipart/form-data') )}}

	                        	<div class="form-group">
	                                <label>Нүүр зураг</label>
	                                <input name="homeImage" type="file" id="image_file" name="file" accept="image/*"><br/>
	                                <div id="selectedFile"></div>

	                                <!-- Modal -->
                                    <div id="homeModal" class="modal fade bs-example-modal-lg" role="dialog" aria-labelledby="gridSystemModalLabel">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button id="homeCloseModal" type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                    <h4 class="modal-title" id="gridSystemModalLabel">Хэмжээ тааруулах</h4>
                                                </div>
                                                <div class="modal-bodyCrop">
                                                    <!-- Crop size -->
                                                    <div class="overlayHome">
                                                        <div class="overlayHome-inner">
                                                        </div>
                                                    </div>
                                                    <!-- .Crop size -->
                                                    <img class="home-resize-image" id="homeImageResizing" src="" alt="image for resizing">
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-primary home-js-crop">Тайрах<img class="icon-crop" src="images/crop.svg"></button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- .Modal -->
	                            </div>

	                           <div class="form-group">
	                           		<label>Гарчиг</label>
			                        <input id="title" name='title' class="form-control">
			                    </div>

			                    <div class="form-group">
	                                <label>Агуулга</label>
	                                <textarea id="content" name='content' class="form-control" rows="8"></textarea>
	                            </div>

	                            <div class="form-group">
	                                <label>Sliper зурагнууд</label>
	                                <input name="sliderImages[]" type="file" id="image_files" multiple accept="image/*"><br/>
	                                <div id="selectedFiles"></div>
	                                <!-- <p class="help-block">Дэлгэрэнгүй хэсэгрүү орсоны дараа харагдах sliper зурагнууд</p> -->
	                            </div>

	                            <hr>
	                            <div id="error"></div>
	                            <button id="btn_rgstrNews" type="submit" class="btn btn-default">Хадаглах</button>

	                        {{Form::token()}}
                            {{Form::close()}}
                        </div>

                    </div>
                    <!-- /.row (nested) -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    
@stop