@extends('back._layout.layout')
@section('content')

    <br>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-recycle"></i> Хандив оруулах
                </div>
                <div class="panel-body">
                    <div class="row">

                        <div class="col-lg-12">

                        	{{Form::open( array('method' => 'post','id'=>'rgstrPost','enctype' => 'multipart/form-data') )}}

                        		<div class="form-group">
                                    <label>Категори</label>
                                    <input value='1' type='hidden' name="category_post_id" id="category_post_id">
                                    <select class="form-control" name="category_post" id="category_post">
                                    	@foreach($categories as $category)
                                			<option class="{{$category->id}}">{{$category->title}}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label>Хандивыг хүсэмжлэгч</label>
                                    <input value='1' type='hidden' name="user_post_id" id="user_post_id">
                                    <select class="form-control" name="usermail_post" id="usermail_post">
                                    	@foreach($users as $user)
                                			<option class="{{$user->id}}">{{$user->email}}</option>
                                        @endforeach
                                    </select>
                                </div>

	                        	<div class="form-group">
	                           		<label>Гарчиг</label>
			                        <input id="title_post" name='title_post' class="form-control">
			                    </div>

			                    <div class="form-group">
	                           		<label>Агуулга</label>
	                           		<textarea class="form-control" rows="8" id="content_post" name="content_post"></textarea>
			                    </div>

			                    <div class="form-group">
	                           		<label>Нүүр зураг</label>
			                        <input name="homeImage_post" type="file" id="image_file_post" name="file" accept="image/*"><br/>
	                                <div id="selectedFile_post"></div>
			                    </div>

			                    <div class="form-group">
	                           		<label>Бичлэгний (embed)</label>
			                        <input id="embed_post" name='embed_post' class="form-control" placeholder="{{Lang::get('category.desc_Url')}}">
			                    </div>

			                    <div class="form-group">
	                           		<label>Мөнгөн дүн (Нийт)</label>
			                        <input id="rec_post" name='rec_post' class="form-control">
			                    </div>

			                    <div class="form-group">
	                           		<label>Мөнгөн дүн (Одоогоор цугларсан байгаа)</label>
			                        <input id="cur_post" name='cur_post' class="form-control">
			                    </div>

			                    <div class="form-group">
	                           		<label>Дуусах хугацаа</label>
			                        <input type="date" id="enddate_post" name='enddate_post' class="form-control">
			                    </div>

			                    <div class="form-group">
	                                <label>Sliper зурагнууд</label>
	                                <input name="sliderImages_post[]" type="file" id="image_files_post" multiple accept="image/*"><br/>
	                                <div id="selectedFiles_post"></div>
	                            </div>

			                    <hr>
	                            <div id="error"></div>
	                            <button id="btn_rgstrPost" type="submit" class="btn btn-default">Хадаглах</button>

	                        {{Form::token()}}
                            {{Form::close()}}
                        </div>

                    </div>
                    <!-- /.row (nested) -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->

@stop