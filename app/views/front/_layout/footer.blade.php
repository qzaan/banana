<!-- footer top -->
<footer class="dark-background footer-top">
    <div class="container">
        <div class="row">

            <!-- footer latest blog -->
            <div class="col-md-9">
                <div class="widget footer-widget">
                    <div class="heading">
                        <h5>{{Lang::get('category.aboutus')}}</h5>
                    </div>
                    <p>
                        {{Lang::get('category.testText10')}}<br /><br />
                    </p>
                    <div class="heading">
                        <h6>{{Lang::get('category.purpose')}}</h6>
                    </div>
                    <p>
                        {{Lang::get('category.testText11')}}
                    </p>
                    <br />
                </div>
            </div>
            <!-- footer latest blog -->

            <!-- footer latest blog -->
            <div class="col-md-3">
                <div class="widget footer-widget">
                    <div class="heading">
                        <h5>{{Lang::get('category.contactUs')}}</h5>
                    </div>
                    <p>{{Lang::get('category.testText9')}}</p>
                    <br />
                    <p><i class="fa fa-map-marker light-grey"></i>&nbsp;
                        <span class="grey">{{Lang::get('info.location')}}</span>
                    </p>
                    <p><i class="fa fa-phone light-grey"></i>&nbsp;
                        <span class="grey">{{Lang::get('info.phone')}}</span>
                    </p>
                    <p><i class="fa fa-envelope-o light-grey"></i>&nbsp;
                        <span class="grey">{{Lang::get('info.mail')}}</span>
                    </p>
                    <p><i class="fa fa-globe light-grey"></i>&nbsp;
                        <span class="grey">{{Lang::get('info.site')}}</span>
                    </p>


                </div>
            </div>
            <!-- footer latest blog -->

        </div>
    </div>
</footer>
<!-- .footer -->