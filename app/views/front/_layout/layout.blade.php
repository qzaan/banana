<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="keywords" content="" />
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">

    <!-- Title -->
    <title>Нэр</title>

    <!-- Favicon -->
    <link rel="shortcut icon" type="image/png" href="/images/favico.png">

    <!-- Google Fonts -->
    <!-- <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'> -->

    <!-- Bootstrap -->
    <link href="/css/bootstrap.min.css" rel="stylesheet">
    <link href="/css/font-awesome.min.css" rel="stylesheet">

    <!-- Lightbox -->
    <link href="/css/dark.css" rel="stylesheet">

    <!-- Template -->
    <link href="/style.css" rel="stylesheet">

    <!-- Custom Color -->
    <link href="/css/color.css" rel="stylesheet">

    <!-- Custom Box -->
    <link href="/css/box.css" rel="stylesheet">

    <!-- Alert -->
    <script src="/js/sweet-alert.min.js"></script>
    <link href="/css/sweet-alert.css" rel="stylesheet">

    <!-- Image crop -->
    <link href="/css/component.css" rel="stylesheet">
</head>

<body>

    <div class="main-wrap">

    <!-- top bar -->
    <header class="top-bar">
        <div class="container">
            <div class="row">

                <!-- languages -->
                <div class="col-md-4 col-xs-6">
                    <div class="languages nav-root">
                        @if(Session::has('lang'))
                            @if(Session::get('lang')=='en')
                            <div class="pt-nav-trigger">
                                <button onclick="javascript:window.location.href='/language?lang=mn'"><i class="fa fa-globe"></i>English</button>
                            </div>
                            <nav class="pt-nav"><ul><li> <a href="/language?lang=mn"><i class="fa fa-globe"></i>English</li></ul></nav>
                            @else
                            <div class="pt-nav-trigger">
                            <button onclick="javascript:window.location.href='/language?lang=en'"><i class="fa fa-globe"></i>Монгол</button>
                            </div>
                            <nav class="pt-nav"><ul><li><a href="/language?lang=en"><i class="fa fa-globe"></i>Монгол</li></ul></nav>
                            @endif
                        @else
                        <div class="pt-nav-trigger">
                            <button onclick="javascript:window.location.href='/language?lang=en'"><i class="fa fa-globe"></i>Монгол</button>
                        </div>
                        <nav class="pt-nav"><ul><li><a href="/language?lang=en"><i class="fa fa-globe"></i>Монгол</li></ul></nav>
                        @endif

                    </div>
                </div>
                <!-- .languages -->

                <!-- add info -->
                <div class="col-md-8 col-xs-12 clearfix">
                    <div class="add-info">

                        <!-- menu list -->
                        <nav>
                            <ul class="list-inline">
                                <li>
                                    <a><i class="fa fa-phone"></i> {{Lang::get('info.phone')}}</a>
                                </li>
                                <li>
                                    <a><i class="fa fa-envelope-o"></i> {{Lang::get('info.mail')}} &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a>
                                </li>
                                <li>
                                    @if(Sentry::check())
                                    <a href="/logout"><i class="fa fa-sign-out"></i> <u> {{Lang::get('category.logout');}}</u></a>
                                    @else
                                    <a href="/login"><i class="fa fa-sign-in"></i> <u> {{Lang::get('category.login');}}</u></a>
                                    @endif
                                </li>
                            </ul>
                            <!-- .menu list -->
                        </nav>

                    </div>
                </div>
                <!-- .add info -->

            </div>
        </div>
    </header>
    <!-- .top bar -->

    <!-- main header -->
    <header class="main-header">

        <div class="container">
            <div class="row">

                <!-- logo -->
                <div class="col-md-2">
                    <div class="logo">
                        <a href="/">
                            <img src="/images/logo.png" title="Лого нэр" alt="Лого нэр" />
                        </a>
                    </div>
                </div>
                <!-- .logo -->

                <!-- main nav -->
                <div class="col-md-10">
                    <div class="main-nav nav-root">

                        <div class="pt-nav-trigger">
                           <button>
                                <i class="fa fa-bars"></i>{{Lang::get('category.menu');}}
                           </button>
                       </div>

                        <!-- trigger -->
                        <nav class="pt-nav clearfix">
                            
                            <!-- menu list -->
                            <ul class="clearfix">
                                @if(Sentry::check())
                                <li>
                                    <a href="/r"><u>{{Lang::get('category.requestDonation');}}</u></a>
                                </li>
                                @endif
                                <li>
                                    <a href="#">{{Lang::get('category.accountInfo');}}</a>
                                </li>
                                <li>
                                    <a href="/tos">{{Lang::get('category.termsOfService');}}</a>
                                </li>
                                <li>
                                    <a href="/n">{{Lang::get('category.news');}}</a>
                                </li>
                                <li>
                                    <a href="/s">{{Lang::get('category.shop');}}</a>
                                </li>
                                <li>
                                    <a href="/c">{{Lang::get('category.contact');}}</a>
                                </li>
                                <li class="button">
                                    <a href="/p">{{Lang::get('category.donate');}}</a>
                                </li>
                            </ul>
                            <!-- .menu list -->

                        </nav>
                    </div>
                </div>
                <!-- .main nav -->

            </div>
        </div>
    </header>
    <!-- .main-header -->

    @yield('content')

     <!-- logo & social -->
    <section class="blue-background">
        <div class="container">
            <div class="row">

                <!-- logo -->
                <div class="col-md-6 col-xs-6 text-left">
                    <div class="logo-white">
                        <a href="#">
                            <img src="/images/logo_white.png" title="" alt="" />
                        </a>
                    </div>
                </div>
                <!-- .logo -->

                <!-- social -->
                <div class="col-md-6 col-xs-6 text-right">
                    <ul class="social list-inline list-unstyled">
                        <li><a href="#"><i class="fa fa-twitter-square"></i></a>
                        </li>
                        <li><a href="#"><i class="fa fa-facebook-square"></i></a>
                        </li>
                        <li><a href="#"><i class="fa fa-google-plus-square"></i></a>
                        </li>
                    </ul>
                </div>
                <!-- .social -->

            </div>
        </div>
    </section>
    <!-- .logo & social -->
    
    <?php
        if( $_SERVER['REQUEST_URI'] === "/" )
        {
            ?>@include('front._layout.footer')<?php
        }
    ?>

    <!-- footer -->
    <footer class="dark-background">
        <div class="container">
            <div class="row">

                <!-- footer menu -->
                <div class="col-md-6 col-xs-6 text-left">
                    <ul class="list-unstyled list-inline">
                        @if(Sentry::check())
                        <li>
                            <a href="/r"><u>{{Lang::get('category.requestDonation');}}</u></a>
                        </li>
                        @endif
                        <li>
                            <a href="#">{{Lang::get('category.accountInfo');}}</a>
                        </li>
                        <li>
                            <a href="/tos">{{Lang::get('category.termsOfService');}}</a>
                        </li>
                        <li>
                            <a href="/n">{{Lang::get('category.news');}}</a>
                        </li>
                        <li>
                            <a href="/s">{{Lang::get('category.shop');}}</a>
                        </li>
                        <li>
                            <a href="/c">{{Lang::get('category.contact');}}</a>
                        </li>
                    </ul>
                </div>
                <!-- footer menu -->

                <!-- copyrights -->
                <div class="col-md-6 col-xs-6 text-right">
                    <p>{{Lang::get('category.madeby')}}</p>
                </div>
                <!-- .copyrights -->
            </div>
        </div>
    </footer>
    <!-- .footer -->

    </div>

    <script src="/js/jquery-2.1.1.min.js"></script>
    <script src="/js/bootstrap.min.js"></script>
    <script src="/js/jquery.downCount.js"></script>
    <script src="/js/bootstrap-slider.js"></script>
    <script src="/js/ekko-lightbox.js"></script>
    <script src="/js/custom.js" id="oldJS"></script>

    <script src="/js/component.js"></script>
    <script src="/js/main_front.js"  id="oldMainJS"></script>
</body>

</html>

