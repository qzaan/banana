<!-- search causes -->
    <div class="col-md-3">

        <!-- search causes widget -->
        <div class="widget">
            <div class="widget-search-causes">
                <div class="box-wrapper">
                    <div class="box">

                        <!-- widget title -->
                        <div class="widget-title">
                            <h5>{{Lang::get('category.causesSearch');}}</h5>
                        </div>
                        <!-- .widget title -->

                        <!-- widget box -->
                        {{Form::open( array('method' => 'post','id'=>'searchAny','enctype' => 'multipart/form-data') )}}
                            <div class="widget-box">
                                <div class="input-group">
                                    <input name="searchValue" type="text" class="form-control" placeholder="{{Lang::get('category.typeCauseName')}}" />
                                    <span class="input-group-addon">
                                        <a id="searchAnyForm" href="#"><i class="fa fa-search"></i></a>
                                    </span>
                                </div>
                            </div>
                        {{Form::token()}}
                        {{Form::close()}}
                        <!-- .widget box -->
                        <hr/>
                        <!-- widget box -->
                        <div class="widget-box dropdown">
                            <div class="widget-dropdown">
                                <a class="button-normal full blue left-text" href="index.html#" data-toggle="dropdown">{{Lang::get('category.categories');}}<i class="fa fa-angle-down pull-right"></i></a>
                                <ul class="dropdown-menu" role="menu">
                                    @foreach($categories as $category)
                                    <li>
                                        <a href="#{{$category->id}}">{{$category->title}}<i class="fa fa-angle-right pull-right"></i></a>
                                    </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                        <!-- .widget-box -->

                        <!-- widget box -->
                        <div class="widget-box dropdown">
                            <!-- rised bar -->
                            <div class="slider-content">
                                <p>
                                    <span>{{Lang::get('category.fundsFrom');}}</span>
                                    <span id="begin-value" class="slide-value">₮0</span>
                                </p>
                                <input type="text" class="span2 rised-range-funds" value="" data-slider-min="0" data-slider-max="200000000" data-slider-step="100000" data-slider-value="0" />

                            </div>
                            <!-- .rised bar -->
                        </div>
                        <!-- .widget-box -->

                        <!-- widget box -->
                        <div class="widget-box dropdown">
                            <!-- rised bar -->
                            <div class="slider-content">
                                <p>
                                    <span>{{Lang::get('category.ageFrom');}}</span>
                                    <span id="begin-value-age" class="slide-value">0 {{Lang::get('category.days');}}</span>
                                </p>
                                <input type="text" class="span2 rised-range-age" value="" data-slider-min="0" data-slider-max="365" data-slider-step="1" data-slider-value="0" />

                            </div>
                            <!-- .rised bar -->
                        </div>
                        <!-- .widget-box -->

                        <!-- widget box -->
                        <div class="widget-box dropdown">
                            <a class="button-normal full white" href="javascript:swal('Уучлаарай','Одоогоор ажиллагаанд ороогүй.');">{{Lang::get('category.search');}}</a>
                        </div>
                        <!-- .widget-box -->
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- .search causes widget -->