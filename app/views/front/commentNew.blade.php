@foreach($comments as $comment)
<!-- 1 -->
<div class="content comments">

    <div class="media">

        <div class="small-product pull-left">
            <div class="small-product-wrapper">
                <img class="media-object img-thumbnail img-circle" src="../images/user/userComment.png" title="" alt="" />
            </div>
        </div>

        <div class="media-body small-product">
            <p class="lead">
                <span class="grey">
                    <!-- <a href="#"> -->
                        {{$comment->title}}
                    <!-- </a> -->
                </span> 
            </p>
            <p>{{$comment->body}}</p>
            <span class="grey">
                <i class="fa fa-clock-o"></i>
                <?php

                    $now = new DateTime();
                    $old = new DateTime( $comment->created_at );
                    $interval = $now->diff($old);

                    $year = $interval->format('%y');
                    $month = $interval->format('%m');
                    $day = $interval->format('%d');
                    $hour = $interval->format('%h');
                    $minute = $interval->format('%i');  

                    $new = 1;
                    if($year != 0){ $new = 0; echo $year . " "; ?>{{Lang::get('category.year')}}<?php }
                    if($month != 0){ $new = 0; echo " " . $month . " "; ?>{{Lang::get('category.month')}}<?php }
                    if($day != 0){ $new = 0; echo " " . $day . " "; ?>{{Lang::get('category.day')}}<?php }
                    if($hour != 0){ $new = 0; echo " " . $hour . " "; ?>{{Lang::get('category.hour')}}<?php }
                    if($minute != 0){ $new = 0; echo " " . $minute . " "; ?>{{Lang::get('category.minute')}}<?php }

                    if($new == 1){ echo " 0 "; ?>{{Lang::get('category.minute')}}<?php }
                ?>

                {{Lang::get('category.ago')}}
            </span>
        </div>
    </div>

</div>

<hr />
<!-- .1 -->
@endforeach