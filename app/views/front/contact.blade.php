@extends('front._layout.layout')
@section('content')

    <!-- breadcrumbs -->
    <section class="breadcrumbs">
        <div class="breadcrumbs-wrapper">
            <div class="container">
                <div class="row">

                    <!-- page title -->
                    <div class="col-md-6 col-xs-6">
                        <h4>{{Lang::get('category.contact')}}</h4>
                    </div>
                    <!-- .page title -->

                    <!-- breadcumbs -->
                    <div class="col-md-6 col-xs-6">
                        <ol class="breadcrumb">
                            <li><a href="/">{{Lang::get('category.home')}}</a>
                            </li>
                            <li class="active">
                                <a href="#">-</a>
                            </li>
                        </ol>
                    </div>
                    <!-- .breadcrumbs -->
                </div>
            </div>
        </div>
    </section>
    <!-- .breadcrumbs -->

    <!-- map -->
    <section>
        <div class="container">
            <div class="row">

                <!-- map -->
                <div class="col-md-12">
                    <div class="box-wrapper">
                        <div class="box">
                            <div class="map">
                                <div class="embed-container ">
                                    <iframe class="map" src="{{Lang::get('info.GoogleMap')}}"></iframe>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- .map -->

            </div>
        </div>
    </section>
    <!-- .map -->

    <!-- contact form & additional info -->
    <section>
        <div class="container">
            <div class="row">

                <!-- contact form -->
                <div class="col-md-8">
                    <div class="widget">
                        <div class="box-wrapper">
                            <div class="box">

                                <!-- heading -->
                                <div class="form-heading">
                                    <h5>{{Lang::get('category.contactUsForm')}}</h5>

                                </div>
                                <!-- heading -->

                                <!-- border -->
                                <hr />
                                <!-- .border -->

                                <!-- form -->
                                <div class="content">
                                    <form class="form-horizontal" role="form">

                                        <!-- widget box -->
                                        <div class="widget-box">
                                            <div class="form-group">
                                                <label>{{Lang::get('category.yourMail')}}</label>
                                                @if(Sentry::check())
                                                <input type="text" class="form-control" value="{{Sentry::getUser()->email}}" readOnly="true"/>
                                                @else
                                                <input type="text" class="form-control" />
                                                @endif
                                            </div>
                                        </div>
                                        <!-- .widget box -->

                                        <!-- widget box -->
                                        <div class="widget-box">
                                            <div class="form-group">
                                                <label>{{Lang::get('category.yourMessage')}}</label>
                                                <textarea class="form-control"></textarea>
                                            </div>
                                        </div>
                                        <!-- .widget box -->

                                        <!-- widget box -->
                                        <div class="widget-box">
                                            <div class="form-group">
                                                <a class="button-normal full white" href="javascript:swal('Уучлаарай','Одоогоор ажиллагаанд ороогүй.')">{{Lang::get('category.send')}}</a>
                                            </div>
                                        </div>
                                        <!-- .widget-box -->

                                    </form>
                                </div>
                                <!-- .form -->


                            </div>
                        </div>
                    </div>
                </div>
                <!-- .contact form -->

                <!-- additional info -->
                <div class="col-md-4">
                    <div class="box-wrapper">
                        <div class="box">
                            <div class="form-heading">
                                <h5>{{Lang::get('category.additionalInfo')}}</h5>
                            </div>
                            <hr />
                            <div class="content">
                                <p>{{Lang::get('category.testText9')}}</p>
                                <br />
                                <p>{{Lang::get('category.address')}}
                                    <span class="pull-right grey">{{Lang::get('info.location')}}</span>
                                </p>
                                <p>{{Lang::get('category.phone')}}
                                    <span class="pull-right grey">{{Lang::get('info.phone')}}</span>
                                </p>
                                <p>{{Lang::get('category.fax')}}
                                    <span class="pull-right grey">{{Lang::get('info.fax')}}</span>
                                </p>
                                <p>{{Lang::get('category.email')}}
                                    <span class="pull-right grey">{{Lang::get('info.mail')}}</span>
                                </p>
                                <p>{{Lang::get('category.web')}}
                                    <span class="pull-right grey">{{Lang::get('info.site')}}</span>
                                </p>


                            </div>
                        </div>
                    </div>
                </div>
                <!-- additional info -->

            </div>
        </div>
    </section>
    <!-- .contact form & additional info -->
    
@stop