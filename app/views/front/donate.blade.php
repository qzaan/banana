@extends('front._layout.layout')
@section('content')

    <!-- breadcrumbs -->
    <section class="breadcrumbs">
        <div class="breadcrumbs-wrapper">
            <div class="container">
                <div class="row">

                    <!-- page title -->
                    <div class="col-md-6 col-xs-6">
                        <h4>{{Lang::get('category.donate')}}</h4>
                    </div>
                    <!-- .page title -->

                    <!-- breadcumbs -->
                    <div class="col-md-6 col-xs-6">
                        <ol class="breadcrumb">
                            <li><a href="#">{{Lang::get('category.home')}}</a>
                            </li>
                            <li class="active"><a href="#">-</a>
                            </li>
                        </ol>
                    </div>
                    <!-- .breadcrumbs -->
                </div>
            </div>
        </div>
    </section>
    <!-- .breadcrumbs -->   

    <!-- donate -->
    <section>
        <div class="container">
            <div class="row">

                <!-- Post -->
                <div class="col-md-6">
                    <div class="box-wrapper">
                        <div class="box">
                            <div class="latest-box">

                                <!-- .media -->
                                <div class="media">

                                    <!-- overlay -->
                                    <div class="overlay-wrapper">
                                        <div class="overlay">
                                            <div class="overlay-content">
                                                <div class="content-hidden">
                                                    <p>{{Lang::get('category.shareThisCause')}}</p>
                                                    <ul class="list-inline list-unstyled">
                                                        <li><a href="#"><i class="fa fa-facebook-square"></i></a>
                                                        </li>
                                                        <li><a href="#"><i class="fa fa-twitter-square"></i></a>
                                                        </li>
                                                        <li><a href="#"><i class="fa fa-google-plus-square"></i></a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- .overlay -->

                                    <!-- image -->
                                    <img src="{{$post->image}}" class="img-responsive" title="" alt="" />
                                    <!-- image -->
                                </div>
                                <!-- .media -->

                                <!-- content -->
                                <div class="content-wrapper">
                                    <div class="content">

                                        <!-- cause meta -->
                                        <div class="meta clearfix">
                                            <a class="pull-left">{{$post->category['title']}}</a>
                                            <a class="pull-right share-trigger" data-rel="tooltip" title="{{Lang::get('category.shareCause')}}" href="javascript:;"><i class="fa fa-share-alt"></i></a>
                                        </div>
                                        <!-- .cause meta -->

                                        <!-- excerpt -->
                                        <div class="excerpt">
                                            <h6>
                                                {{$post->title}}
                                            </h6>

                                        </div>
                                        <!-- .excerpt -->

                                        <!-- rised bar -->
                                        <div class="slider-content">
                                            <input class="rised" data-slider-id='ex1Slider' type="text" data-slider-min="0" data-slider-max="{{$post->rec}}" data-slider-step="1" data-slider-enabled="false" data-slider-value="{{$post->cur}}" />
                                        </div>
                                        <!-- .rised bar -->

                                        <!-- rised slider meta -->
                                        <div class="slider-meta clearfix">
                                            <span class="pull-left">₮<?php echo number_format($post->cur) ?></span>
                                            <span class="pull-right">₮<?php echo number_format($post->rec) ?></span>
                                        </div>
                                        <!-- .rised slider meta -->


                                    </div>
                                </div>
                                <!-- .content -->

                            </div>
                        </div>
                    </div>
                </div>
                <!-- .Post -->

                <!-- map --><!-- col-md-push-3 -->
                <div class="col-md-6">
                    <div class="box-wrapper">
                        <div class="box">
                            <div class="donate-form">

                                <div class="content comments">
                                    <h5 class="grey">{{Lang::get('category.donationToCause')}}</h5>
                                    <h4>{{Lang::get('category.testText7')}}</h4>
                                </div>
                                <hr class="inline-hr" />

                                <!-- form -->
                                <div class="content">
                                    <form role="form">

                                        <!-- widget box -->
                                        <div class="widget-box">
                                            <div data-toggle="buttons">
                                                <label class="btn btn-circle btn-primary active">
                                                    <input type="radio" name="options">₮500
                                                </label>
                                                <label class="btn btn-primary">
                                                    <input type="radio" name="options">₮1000
                                                </label>
                                                <label class="btn btn-primary">
                                                    <input type="radio" name="options">₮5000
                                                </label>
                                                <label class="btn btn-primary">
                                                    <input type="radio" name="options">₮10000
                                                </label>
                                                <label class="btn btn-primary">
                                                    <input type="radio" name="options">₮20000
                                                </label>
                                            </div>
                                        </div>
                                        <!-- .widget box -->

                                        <!-- widget box -->
                                        <div class="widget-box">
                                            <div class="form-group">
                                                <input type="text" class="form-control" placeholder="{{Lang::get('category.custommount')}}" />
                                            </div>
                                        </div>
                                        <!-- .widget box -->

                                        <!-- widget box -->
                                        <div class="widget-box">
                                            <div class="form-group row">
                                                <div class="col-md-6">
                                                    <input type="text" class="form-control" placeholder="{{Lang::get('category.firstName')}}" />
                                                </div>
                                                <div class="col-md-6">
                                                    <input type="text" class="form-control" placeholder="{{Lang::get('category.lastName')}}" />
                                                </div>
                                            </div>
                                        </div>
                                        <!-- .widget box -->

                                        <!-- widget box -->
                                        <div class="widget-box">
                                            <div class="form-group row">
                                                <div class="col-md-6">
                                                    <input type="text" class="form-control" placeholder="{{Lang::get('category.yourMail')}}" />
                                                </div>
                                                <div class="col-md-6">
                                                    <input type="text" class="form-control" placeholder="{{Lang::get('category.yourPhone')}}" />
                                                </div>
                                            </div>
                                        </div>
                                        <!-- .widget box -->

                                        <!-- widget box -->
                                        <div class="widget-box">
                                            <div class="form-group">
                                                <textarea class="form-control" placeholder="{{Lang::get('category.testText8')}}"></textarea>
                                            </div>
                                            <!-- <span class="light-grey">{{Lang::get('category.charactersAllowed')}}</span> -->
                                        </div>
                                        <!-- .widget box -->

                                        <!-- widget box -->
                                        <div class="widget-box">
                                                    <input type="checkbox"> {{Lang::get('category.anonymusDonation')}} <span class="light-grey">({{Lang::get('category.notPublic')}})</span>
                                        </div>
                                        <!-- .widget box -->

                                        <!-- widget box -->
                                     
                                            <div class="form-group">
                                                <a class="button-normal full blue" href="#">{{Lang::get('category.donateUpper')}}</a>
                                            </div>
                                       
                                        <!-- .widget-box -->

                                    </form>
                                </div>
                                <!-- .form -->

                            </div>
                        </div>
                    </div>
                </div>
                <!-- .map -->

                
            </div>
        </div>
    </section>
    <!-- .donate -->

@stop