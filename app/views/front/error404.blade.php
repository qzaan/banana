@extends('front._layout.layout')
@section('content')

    <!-- breadcrumbs -->
    <section class="breadcrumbs">
        <div class="breadcrumbs-wrapper">
            <div class="container">
                <div class="row">

                    <!-- page title -->
                    <div class="col-md-6 col-xs-6">
                        <h4>{{Lang::get('category.errorPage')}}</h4>
                    </div>
                    <!-- .page title -->

                    <!-- breadcumbs -->
                    <div class="col-md-6 col-xs-6">
                        <ol class="breadcrumb">
                            <li><a href="#">{{Lang::get('category.home')}}</a>
                            </li>
                            <li class="active"><a href="#">404</a>
                            </li>
                        </ol>
                    </div>
                    <!-- .breadcrumbs -->
                </div>
            </div>
        </div>
    </section>
    <!-- .breadcrumbs -->

    <!-- map -->
    <section>
        <div class="container">
            <div class="row">

                <!-- map -->
                <div class="col-md-12">
                    <div class="box-wrapper">
                        <div class="box">

                            <!-- 404 -->
                            <div class="content">
                                <h2>404 {{Lang::get('category.errorPage')}}</h2>
                                <h6>{{Lang::get('category.testText4')}}</h6>
                            </div>
                            <hr class="inline-hr" />
                            <div class="content">
                                <form action="#" method="get">
                                    <!-- widget box -->
                                    <div class="widget-box">
                                        <p>{{Lang::get('category.testText5')}}</p>
                                        <div class="input-group">
                                            <input type="text" class="form-control" placeholder="{{Lang::get('category.typeCauseName')}}" />
                                            <span class="input-group-addon">
                                                <a href="#"><i class="fa fa-search"></i></a>
                                            </span>
                                        </div>
                                    </div>
                                    <!-- .widget box -->
                                </form>
                            </div>
                            <!-- .404 -->

                        </div>
                    </div>
                </div>
                <!-- .map -->

            </div>
        </div>
    </section>
    <!-- .map -->
@stop