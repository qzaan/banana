@extends('front._layout.layout')
@section('content')
    <!-- slider -->
    <aside>

        <div id="myCarousel" class="carousel slide" data-ride="carousel" data-interval="6000">


            <!-- Indicators -->
            <ol class="carousel-indicators">
                <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                <li data-target="#myCarousel" data-slide-to="1"></li>
                <li data-target="#myCarousel" data-slide-to="2"></li>
                <li data-target="#myCarousel" data-slide-to="3"></li>
            </ol>


            <!-- Wrapper for slides -->
            <div class="carousel-inner">

                <!-- 1 -->
                <div class="item active">

                    <img src="http://lorempixel.com/1920/532/" alt="" title="" />

                    <div class="carousel-caption">
                        <div class="carousel-content-wrapper">
                            <div class="carousel-content slide-content">
                                <h3>Charity Fundrising</h3>
                                <p>Donate to help everyone who needs help</p>
                            </div>
                        </div>
                    </div>

                </div>
                <!-- .1 -->

                <!-- Slide 2 -->
                <div class="item">

                    <img src="http://lorempixel.com/1920/532/" alt="" title="" />

                    <div class="carousel-caption">
                        <div class="carousel-content-wrapper slide-content">
                            <div class="carousel-content">
                                <h3>Give Them Hope</h3>
                                <p>The charity that is a trifle to us can be precious to others</p>
                            </div>
                        </div>
                    </div>
                    <!-- /.carousel-caption -->
                </div>
                <!-- /Slide2 -->

                <!-- Slide 3 -->
                <div class="item ">

                    <img src="http://lorempixel.com/1920/532/" alt="" title="" />

                    <div class="carousel-caption">
                        <div class="carousel-content-wrapper">
                            <div class="carousel-content slide-content">
                                <h3>Provide Better Future</h3>
                                <p>Anticipate charity by preventing poverty</p>
                            </div>
                        </div>
                    </div>
                    <!-- /.carousel-caption -->
                </div>
                <!-- /Slide3 -->

                <!-- Slide 4 -->
                <div class="item">

                    <img src="http://lorempixel.com/1920/532/" alt="" title="" />

                    <div class="carousel-caption">
                        <div class="carousel-content-wrapper">
                            <div class="carousel-content slide-content">
                                <h3>More Love</h3>
                                <p>Charity is willingly given from the heart</p>
                            </div>
                        </div>
                    </div>
                    <!-- /.carousel-caption -->
                </div>
                <!-- /Slide4 -->

            </div>
            <!-- /Wrapper for slides .carousel-inner -->

            <!-- Controls -->
            <div class="control-box">
                <a class="left carousel-control" href="#myCarousel" data-slide="prev"><i class="fa fa-angle-left"></i></a>
                <a class="right carousel-control" href="#myCarousel" data-slide="next"><i class="fa fa-angle-right"></i></a>
            </div>
            <!-- /.control-box -->


        </div>
        <!-- /#myCarousel -->

    </aside>
    <!-- .slider -->

    <!-- upcoming event -->
    <section class="up-event">
        <div class="container">
            <div class="row">
                <div class="col-md-12">

                    <!-- box -->
                    <div class="box-wrapper">
                        <div class="box clearfix">

                            <!-- upcoming event -->
                            <div class="upcoming-event">
                                <div class="event-icon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <div class="event-title">
                                    <h3><a href="#">Сайн үйлсийн төлөөх уулзалт</a>
                                    </h3>
                                </div>

                                <div class="event-counter pull-right">

                                    <ul class="countdown list-unstyled list-inline text-center">
                                        <li>
                                            <p class="days_ref">{{Lang::get('category.e_days')}}</p>
                                            <span class="days">00</span>

                                        </li>

                                        <li>
                                            <p class="hours_ref">{{Lang::get('category.e_hours')}}</p>
                                            <span class="hours">00</span>

                                        </li>

                                        <li>
                                            <p class="minutes_ref">{{Lang::get('category.e_minutes')}}</p>
                                            <span class="minutes">00</span>

                                        </li>

                                        <li>
                                            <p class="seconds_ref">{{Lang::get('category.e_seconds')}}</p>
                                            <span class="seconds">00</span>

                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <!-- .upcoming event -->

                        </div>
                    </div>
                    <!-- .box -->

                </div>
            </div>
        </div>
    </section>
    <!-- .upcoming-event -->

    <!-- urgent cause and search -->
    <section>
        <div class="container">
            <div class="row">

                <!-- urgent cause -->
                <div class="col-md-9">

                    <!-- heading -->
                    <div class="heading">

                        <!-- title -->
                        <h3>{{Lang::get('category.urgentCauses');}}</h3>
                        <!-- .title -->

                        <!-- carousel slide -->
                        <div class="carousel-control carousel-control-heading">
                            <a class="carousel-control left" href="#urgentCarousel" data-slide="prev"><i class="fa fa-angle-left"></i></a>
                            <a class="carousel-control right" href="#urgentCarousel" data-slide="next"><i class="fa fa-angle-right"></i></a> 
                        </div>

                        <div class="border">
                            <div class="border-inner">
                            </div>
                        </div>
                        <!-- .carousel slide -->

                    </div>
                    <!-- .heading -->

                    <!-- urgent slider -->
                    <div class="carousel-cause">
                        <div id="urgentCarousel" class="carousel normal slide" data-ride="carousel" data-interval="false">
                            <div class="carousel-inner">

                                <?php
                                    $i = 0;
                                ?>
                                @foreach($postsFast as $post)
                                @if($i == 0)
                                <div class="item active">
                                @else
                                <div class="item">
                                @endif
                                    <div class="box-wrapper">
                                        <div class="box">

                                            <!-- urgent box -->
                                            <div class="urgent-box clearfix">

                                                <!-- .media -->
                                                <div class="media pull-left">

                                                    <!-- overlay -->
                                                    <div class="overlay-wrapper">
                                                        <div class="overlay">
                                                            <div class="overlay-content">
                                                                <div class="content-hidden">
                                                                    <p class="lead">{{Lang::get('category.shareThisCause')}}</p>
                                                                    <ul class="list-inline list-unstyled">
                                                                        <li><a href="#"><i class="fa fa-facebook-square"></i></a>
                                                                        </li>
                                                                        <li><a href="#"><i class="fa fa-twitter-square"></i></a>
                                                                        </li>
                                                                        <li><a href="#"><i class="fa fa-google-plus-square"></i></a>
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- .overlay -->

                                                    <!-- image -->
                                                    <a href="/p/{{$post->id}}">
                                                        <img src="{{$post->image}}" class="img-responsive" title="" alt="" />
                                                    </a>
                                                    <!-- image -->
                                                </div>
                                                <!-- .media -->

                                                <!-- content -->
                                                <div class="content-wrapper pull-right">
                                                    <div class="content">

                                                        <!-- cause meta -->
                                                        <div class="meta clearfix">
                                                            <a class="pull-left" href="#">{{$post->category}}</a>
                                                            <a class="pull-right share-trigger" data-rel="tooltip" title="{{Lang::get('category.shareCause');}}" href="javascript:;"><i class="fa fa-share-alt"></i></a>
                                                        </div>
                                                        <!-- .cause meta -->

                                                        <!-- excerpt -->
                                                        <div class="excerpt">
                                                            <?php

                                                                $buffer_title = $post->title;
                                                                $buffer_body = $post->body;

                                                                $buffer_title = (strlen($buffer_title) >= 43) ? mb_substr($buffer_title,0,43).'...' : $buffer_title;
                                                                $buffer_title_first = mb_substr($buffer_title,0,1);
                                                                $buffer_title_other = mb_substr($buffer_title,1,strlen($buffer_title));

                                                                $buffer_title_first = mb_strtoupper($buffer_title_first);
                                                                $buffer_title_other = mb_strtolower($buffer_title_other);

                                                                $buffer_title_changed = $buffer_title_first . $buffer_title_other;

                                                                $buffer_body = (strlen($buffer_body) >= 228) ? mb_substr($buffer_body,0,228).'...' : $buffer_body;
                                                                $i++;
                                                            ?>
                                                            <h5><a href="/p/{{$post->id}}">{{$buffer_title_changed}}</a>
                                                            </h5>
                                                            <p>{{$buffer_body}}</p>
                                                        </div>
                                                        <!-- .excerpt -->

                                                        <!-- rised bar -->
                                                        <div class="slider-content">
                                                            <input class="rised" data-slider-id='ex1Slider' type="text" data-slider-value="{{$post->cur}}" data-slider-min="0" data-slider-max="{{$post->rec}}" data-slider-step="1" data-slider-enabled="false" />
                                                        </div>
                                                        <!-- .rised bar -->

                                                        <!-- rised slider meta -->
                                                        <div class="slider-meta clearfix">
                                                            <span class="pull-left">₮<?php echo number_format($post->cur) ?></span>
                                                            <span class="pull-right">₮<?php echo number_format($post->rec) ?></span>
                                                        </div>
                                                        <!-- .rised slider meta -->

                                                        <!-- donate button -->
                                                        <div class="urgent-donate">
                                                            <a class="button-normal full blue" href="/d/{{$post->id}}">{{Lang::get('category.donate');}}</a>
                                                        </div>
                                                        <!-- .donate button -->

                                                    </div>
                                                </div>
                                                <!-- .content -->

                                            </div>
                                            <!-- .urgent box -->

                                        </div>
                                    </div>
                                </div>
                                @endforeach

                            </div>
                        </div>
                        <!-- .urgent slider -->

                    </div>
                </div>
                <!-- .urgent cause -->

                @include('front._layout.search')

            </div>
        </div>
        <!-- .search causes -->

    </section>
    <!-- .urgent cause and search -->


    <!-- latest causes -->
    <section>
        <div class="container">
            <div class="row">
                <div class="col-md-12">

                    <!-- heading -->
                    <div class="heading">

                        <!-- title -->
                        <h3>{{Lang::get('category.latestCauses')}}</h3>
                        <!-- .title -->

                        <!-- carousel slide -->
                        <div class="carousel-control carousel-control-heading">
                            <a class="carousel-control left" href="#latestCarousel" data-slide="prev"><i class="fa fa-angle-left"></i></a>
                            <a class="carousel-control right" href="#latestCarousel" data-slide="next"><i class="fa fa-angle-right"></i></a> 
                        </div>

                        <div class="border">
                            <div class="border-inner">
                            </div>
                        </div>

                    </div>
                    <!-- .heading -->

                    <!-- latest causes carousel -->
                    <div id="latestCarousel" class="carousel normal default slide" data-interval="false">
                        <div class="carousel-inner">

                            <?php
                                $i = 0;
                                $j = 0;
                                $pagination_count = 4;
                            ?>
                            @foreach($postsLast as $postLast)
                            <!-- 1 -->
                                @if($i == 0)
                                <div class="item active">
                                        <div class="row">
                                @elseif($j == $pagination_count)
                                <div class="item">
                                    <div class="row">
                                    <?php
                                        $j = 0;
                                    ?>
                                @endif
                                    <!-- a -->
                                    <div class="col-md-3">
                                        <div class="box-wrapper">
                                            <div class="box">
                                                <div class="latest-box">

                                                    <!-- .media -->
                                                    <div class="media">

                                                        <!-- overlay -->
                                                        <div class="overlay-wrapper">
                                                            <div class="overlay">
                                                                <div class="overlay-content">
                                                                    <div class="content-hidden">
                                                                        <p>{{Lang::get('category.shareThisCause')}}</p>
                                                                        <ul class="list-inline list-unstyled">
                                                                            <li><a href="#"><i class="fa fa-facebook-square"></i></a>
                                                                            </li>
                                                                            <li><a href="#"><i class="fa fa-twitter-square"></i></a>
                                                                            </li>
                                                                            <li><a href="#"><i class="fa fa-google-plus-square"></i></a>
                                                                            </li>
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- .overlay -->

                                                        <!-- image -->
                                                        <a href="/p/{{$postLast->id}}">
                                                            <img src="{{$postLast->image}}" class="img-responsive" title="" alt="" />
                                                        </a>
                                                        <!-- image -->
                                                    </div>
                                                    <!-- .media -->

                                                    <!-- content -->
                                                    <div class="content-wrapper">
                                                        <div class="content">

                                                            <!-- cause meta -->
                                                            <div class="meta clearfix">
                                                                <a class="pull-left" href="#">{{$postLast->category}}</a>
                                                                <a class="pull-right share-trigger" data-rel="tooltip" title="{{Lang::get('category.shareCause')}}" href="javascript:;"><i class="fa fa-share-alt"></i></a>
                                                            </div>
                                                            <!-- .cause meta -->

                                                            <!-- excerpt -->
                                                            <div class="excerpt">
                                                                <?php
                                                                    $buffer_title_last = $postLast->title;
                                                                    $buffer_title_last = (strlen($buffer_title_last) >= 36) ? mb_substr($buffer_title_last,0,36).'...' : $buffer_title_last;
                                                                    $buffer_title_first = mb_substr($buffer_title_last,0,1);
                                                                    $buffer_title_other = mb_substr($buffer_title_last,1,strlen($buffer_title_last));

                                                                    $buffer_title_first = mb_strtoupper($buffer_title_first);
                                                                    $buffer_title_other = mb_strtolower($buffer_title_other);

                                                                    $buffer_title_changed = $buffer_title_first . $buffer_title_other;

                                                                    $j++;

                                                                ?>
                                                                <h6><a href="/p/{{$postLast->id}}">{{$buffer_title_changed}}</a>
                                                                </h6>
                                                            </div>
                                                            <!-- .excerpt -->

                                                            <!-- rised bar -->
                                                            <div class="slider-content">
                                                                <input class="rised" data-slider-id='ex1Slider' type="text" data-slider-min="0" data-slider-max="{{$postLast->rec}}" data-slider-step="1" data-slider-enabled="false" data-slider-value="{{$postLast->cur}}" />
                                                            </div>
                                                            <!-- .rised bar -->

                                                            <!-- rised slider meta -->
                                                            <div class="slider-meta clearfix">
                                                                <span class="pull-left">₮<?php echo number_format($postLast->cur) ?></span>
                                                                <span class="pull-right">₮<?php echo number_format($postLast->rec) ?></span>
                                                            </div>
                                                            <!-- .rised slider meta -->


                                                        </div>
                                                    </div>
                                                    <!-- .content -->

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- .a -->
                            @if($j == $pagination_count)
                                </div>
                            </div>
                            @endif
                            <?php
                                $i++;
                            ?>
                            <!-- .1 -->
                            @endforeach
                        </div>
                    </div>
                    <!-- .latest causes carousel -->

                </div>
            </div>
        </div>

    </section>
    <!-- .latest causes -->

    <!-- full width promo -->
    <section class="box-section">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="full-promo text-center">
                        <h2>
                            <strong>{{Lang::get('category.testText1')}}</strong>
                        </h2>
                        <h6>{{Lang::get('category.testText2')}}</h6>
                        <a class="button-normal blue" href="/p">{{Lang::get('category.donate')}}</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- .full width promo -->


    <!-- latest blog & events & newsletter -->
    <section>
        <div class="container">
            <div class="row">

                <!-- latest blog -->
                <div class="col-md-8">

                    <!-- heading -->
                    <div class="heading">

                        <!-- title -->
                        <h3>{{Lang::get('category.newsLatest')}}</h3>
                        <!-- .title -->

                        <!-- carousel slide -->
                        <div class="carousel-control carousel-control-heading">
                            <a class="carousel-control left" href="#blogCarousel" data-slide="prev"><i class="fa fa-angle-left"></i></a>
                            <a class="carousel-control right" href="#blogCarousel" data-slide="next"><i class="fa fa-angle-right"></i></a> 
                        </div>

                        <div class="border">
                            <div class="border-inner">
                            </div>
                        </div>

                    </div>
                    <!-- .heading -->

                    <!-- latest causes carousel -->
                    <div id="blogCarousel" class="carousel normal default slide" data-interval="false">
                        <div class="carousel-inner">

                            <?php
                                $i = 0;
                                $j = 0;
                                $pagination_count = 2;
                            ?>
                            @foreach($news as $new)
                            @if($i == 0)
                            <div class="item active">
                                <div class="row">
                            @elseif($j == $pagination_count)
                            <div class="item">
                                <div class="row">
                            @endif
                                    <!-- date -->
                                    <div class="col-md-2 col-xs-2">
                                        <div class="date">
                                            <?php
                                                $weekday = date('Y', strtotime(str_replace('-','/', $new->created_at)));
                                                $month = date('m', strtotime($new->created_at));
                                                $day = date('d', strtotime($new->created_at));
                                            ?>
                                            <p class="month">{{$month}}</p>
                                            <p class="day">{{$day}}</p>
                                            <p class="year">{{$weekday}}</p>
                                        </div>
                                    </div>
                                    <!-- .date -->

                                    <!-- post -->
                                    <div class="col-md-4 col-xs-4">
                                        <div class="box-wrapper">
                                            <div class="box">
                                                <div class="latest-box">

                                                    <!-- .media -->
                                                    <div class="media">

                                                        <!-- overlay -->
                                                        <div class="overlay-wrapper">
                                                            <div class="overlay">
                                                                <div class="overlay-content">
                                                                    <div class="content-hidden">
                                                                        <p>{{Lang::get('category.shareThisNews')}}</p>
                                                                        <ul class="list-inline list-unstyled">
                                                                            <li><a href="#"><i class="fa fa-facebook-square"></i></a>
                                                                            </li>
                                                                            <li><a href="#"><i class="fa fa-twitter-square"></i></a>
                                                                            </li>
                                                                            <li><a href="#"><i class="fa fa-google-plus-square"></i></a>
                                                                            </li>
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- .overlay -->

                                                        <!-- image -->
                                                        <a href="/n/{{$new->id}}">
                                                            <img src="{{$new->image}}" class="img-responsive" title="" alt="" />
                                                        </a>
                                                        <!-- image -->
                                                    </div>
                                                    <!-- .media -->

                                                    <!-- content -->
                                                    <div class="content-wrapper">
                                                        <div class="content">

                                                            <!-- cause meta -->
                                                            <div class="meta clearfix">
                                                                <a class="pull-left" href="#">Tag эсвэл Category</a>
                                                                <a class="pull-right share-trigger" data-rel="tooltip" title="{{Lang::get('category.shareCause')}}" href="javascript:;"><i class="fa fa-share-alt"></i></a>
                                                            </div>
                                                            <!-- .cause meta -->

                                                            <!-- excerpt -->
                                                            <div class="excerpt">
                                                                <h6>
                                                                    <?php
                                                                        $buffer_title = $new->title;
                                                                        $buffer_title = (strlen($buffer_title) >= 30) ? mb_substr($buffer_title,0,30).'...' : $buffer_title;
                                                                        $buffer_title_first = mb_substr($buffer_title,0,1);
                                                                        $buffer_title_other = mb_substr($buffer_title,1,strlen($buffer_title));

                                                                        $buffer_title_first = mb_strtoupper($buffer_title_first);
                                                                        $buffer_title_other = mb_strtolower($buffer_title_other);

                                                                        $buffer_title_changed = $buffer_title_first . $buffer_title_other;
                                                                
                                                                        $j++;

                                                                    ?>
                                                                    <a href="/n/{{$new->id}}">{{$buffer_title_changed}}</a>
                                                                </h6>

                                                            </div>
                                                            <!-- .excerpt -->

                                                            <!-- rised slider meta -->
                                                            <div class="slider-meta clearfix">

                                                            </div>
                                                            <!-- .rised slider meta -->


                                                        </div>
                                                    </div>
                                                    <!-- .content -->

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- .post -->
                            @if($j == $pagination_count)
                                </div>
                            </div>
                            @endif
                            <?php
                                $i++;
                            ?>
                            @endforeach

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- .latest blog -->

                <!-- subscribe for newsletter -->
                <div class="col-md-4">

                    <!-- newsletter -->
                    <div class="widget">
                        <div class="widget-search-causes">
                            <div class="box-wrapper">
                                <div class="box">

                                    <!-- widget title -->
                                    <div class="widget-title">
                                        <h5>{{Lang::get('category.newsletter')}}</h5>
                                        <p>{{Lang::get('category.newsletterText')}}</p>
                                    </div>
                                    <!-- .widget title -->

                                    <!-- widget box -->
                                    <div class="widget-box">
                                        <div class="input-group">
                                            @if(Sentry::check())
                                            <input value="{{Sentry::getUser()->email}}" readOnly="true" type="text" class="form-control" placeholder="{{Lang::get('category.emailAddress')}}" />
                                            @else
                                            <input type="text" class="form-control" placeholder="{{Lang::get('category.emailAddress')}}" />
                                            @endif
                                            <span class="input-group-addon">
                                                <i class="fa fa-envelope-o"></i>
                                            </span>
                                        </div>
                                    </div>
                                    <!-- .widget box -->

                                    <!-- widget box -->
                                    <div class="widget-box">
                                        <div class="input-group">
                                            <input type="text" class="form-control" placeholder="{{Lang::get('category.firstName')}}" />
                                            <span class="input-group-addon">
                                                <i class="fa fa-user"></i>
                                            </span>
                                        </div>
                                    </div>
                                    <!-- .widget box -->
                                    <!-- widget box -->
                                    <div class="widget-box">
                                        <div class="input-group">
                                            <input type="text" class="form-control" placeholder="{{Lang::get('category.lastName')}}" />
                                            <span class="input-group-addon">
                                                <i class="fa fa-user-md"></i>
                                            </span>
                                        </div>
                                    </div>
                                    <!-- .widget box -->

                                    <!-- widget box -->
                                    <div class="widget-box dropdown">
                                        <a class="button-normal full white" href="javascript:swal('Уучлаарай','Одоогоор ажиллагаанд ороогүй.');">{{Lang::get('category.subscribe')}}</a>
                                    </div>
                                    <!-- .widget-box -->
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- .newsletter -->

                </div>
                <!-- .subscribe for newsletter -->

            </div>
        </div>
    </section>
    <!-- .latest blog & events & newsletter -->


    <!-- latest from shop & gallery -->
    <section>
        <div class="container">
            <div class="row">

                <!-- latest shop -->
                <div class="col-md-12">

                    <!-- heading -->
                    <div class="heading">

                        <!-- title -->
                        <h3>{{Lang::get('category.shopLatest')}}</h3>
                        <!-- .title -->

                        <!-- carousel slide -->
                        <div class="carousel-control carousel-control-heading">
                            <a class="carousel-control left" href="#shopCarousel" data-slide="prev"><i class="fa fa-angle-left"></i></a>
                            <a class="carousel-control right" href="#shopCarousel" data-slide="next"><i class="fa fa-angle-right"></i></a> 
                        </div>

                        <div class="border">
                            <div class="border-inner">
                            </div>
                        </div>

                    </div>
                    <!-- .heading -->

                    <!-- latest causes carousel -->
                    <div id="shopCarousel" class="carousel normal default slide" data-interval="false">
                        <div class="carousel-inner">

                            <?php
                                $i = 0;
                                $j = 0;
                                $pagination_count = 3;
                            ?>
                            @foreach($products as $product)
                            @if($i == 0)
                            <div class="item active">
                                <div class="row">
                            @elseif($j == $pagination_count)
                            <div class="item">
                                <div class="row">
                            @endif
                                    <!-- post -->
                                    <div class="col-md-4">
                                        <div class="box-wrapper">
                                            <div class="box">
                                                <div class="latest-box">

                                                    <!-- content -->
                                                    <div class="content-wrapper">
                                                        <div class="content">

                                                            <!-- cause meta -->
                                                            <div class="meta clearfix">
                                                                <a class="pull-left">₮<?php echo number_format($product->price) ?></a>

                                                                <!-- hidden values -->
                                                                <h4 id="proPrice" class="hidden">₮{{$product->price}}</h4>
                                                                <input id="proId" type="hidden" value="{{$product->id}}" />
                                                                <span id="pick" class="hidden">{{$product->size}}</span>
                                                                <h4 id="proTitle" class="hidden">{{$product->title}}</h4>
                                                                <div class="increment hidden">
                                                                    <input type="hidden" class="form-control" value="1" readonly>
                                                                </div>
                                                                <!-- .hidden values -->

                                                                <a onclick="pushCart()" class="pull-right cart" data-rel="tooltip" title="{{Lang::get('category.addToCart')}}" href="javascript:void(0)"><i class="fa fa-shopping-cart"></i></a>
                                                            </div>
                                                            <!-- .cause meta -->

                                                            <!-- excerpt -->
                                                            <div class="excerpt">
                                                                <h6>
                                                                    <?php
                                                                        $buffer_title = $product->title;
                                                                        $buffer_title = (strlen($buffer_title) >= 32) ? mb_substr($buffer_title,0,32).'...' : $buffer_title;
                                                                        $buffer_title_first = mb_substr($buffer_title,0,1);
                                                                        $buffer_title_other = mb_substr($buffer_title,1,strlen($buffer_title));

                                                                        $buffer_title_first = mb_strtoupper($buffer_title_first);
                                                                        $buffer_title_other = mb_strtolower($buffer_title_other);

                                                                        $buffer_title_changed = $buffer_title_first . $buffer_title_other;

                                                                        $j++;
                                                                    ?>
                                                                    <a href="/s/{{$product->id}}">{{$buffer_title_changed}}</a>
                                                                </h6>
                                                            </div>
                                                            <!-- .excerpt -->

                                                        </div>
                                                    </div>
                                                    <!-- .content -->

                                                    <!-- .media -->
                                                    <div class="media">

                                                        <!-- image -->
                                                        <a href="/s/{{$product->id}}" class="shop">
                                                            <div class="sale">{{Lang::get('category.sale')}}</div>
                                                            <img id="image" src="{{$product->image}}" class="img-responsive" title="" alt="" />
                                                        </a>
                                                        <!-- image -->

                                                    </div>
                                                    <!-- .media -->

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- .post -->

                            @if($j == $pagination_count)
                                </div>
                            </div>
                            @endif
                            <?php
                                $i++;
                            ?>
                            @endforeach
                        </div>
                    </div>
                </div>
                <!-- .latest shop -->

            </div>
        </div>
    </section>
    <!-- .latest from shop & gallery -->

   @stop