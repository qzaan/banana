@extends('front._layout.layout')
@section('content')

    <section>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="promo-box heading text-center">
                        <h2>{{Lang::get('category.login')}}</h2>
                        <p class="lead">Та нэвтрэхийн тулд доорх гурван веб сайтаар бүртгүүлэх боломжтой</p>
                        <hr />
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- intro -->
    <section>
        <div class="container">
            <div class="row">
                <!-- 1 -->
                <div class="col-md-4">
                    <div class="promo-box">
                        <a class="hvr-float-shadow" href="{{{ URL::route('google') }}}">
                            <i class="fa fa-google-plus fa fa-lg"> | {{Lang::get('category.connectGoogle')}}</i> 
                        </a>
                    </div>
                </div>
                <!-- .1 -->

                <!-- 2 -->
                <div class="col-md-4">
                    <div class="promo-box middle">
                        <a class="hvr-float-shadow" href="{{{ URL::route('facebook') }}}">
                            <i class="fa fa-facebook fa fa-lg"> | {{Lang::get('category.connectFacebook')}}</i>
                        </a>
                    </div>
                </div>
                <!-- .2 -->

                <!-- 3 -->
                <div class="col-md-4">
                    <div class="promo-box">
                        <a class="hvr-float-shadow" href="{{{ URL::route('twitter') }}}">
                            <i class="fa fa-twitter fa fa-lg"> | {{Lang::get('category.connectTwitter')}}</i>     
                        </a>
                    </div>
                </div>
                <!-- .3 -->

            </div>
        </div>
    </section>
    <!-- .intro -->

@stop