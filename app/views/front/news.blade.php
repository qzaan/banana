@extends('front._layout.layout')
@section('content')
    
    <!-- breadcrumbs -->
    <section class="breadcrumbs">
        <div class="breadcrumbs-wrapper">
            <div class="container">
                <div class="row">

                    <!-- page title -->
                    <div class="col-md-6 col-xs-6">
                        <h4>{{Lang::get('category.news')}}</h4>
                    </div>
                    <!-- .page title -->

                    <!-- breadcumbs -->
                    <div class="col-md-6 col-xs-6">
                        <ol class="breadcrumb">
                            <li><a href="/">{{Lang::get('category.home')}}</a>
                            </li>
                            <li class="active">
                                <a href="/n">{{Lang::get('category.news')}}</a>
                            </li>
                        </ol>
                    </div>
                    <!-- .breadcrumbs -->
                </div>
            </div>
        </div>
    </section>
    <!-- .breadcrumbs -->

    <!-- blog posts & widgets -->
    <section>
        <div class="container">
            <div class="row">

                <!-- paginate & posts -->
                <div class="col-md-12">
                    
                    <?php echo $news->links(); ?>

                    <!-- posts -->
                    <div class="posts" id="news">
                        @foreach(array_chunk($news->getCollection()->all(),3) as $row)
                        <div class="row">

                            @foreach($row as $new)
                            <!-- 1 -->
                            <div class="col-md-4">
                                <div class="box-wrapper">
                                    <div class="box">
                                        <div class="latest-box">

                                            <!-- .media -->
                                            <div class="media">

                                                <!-- overlay -->
                                                <div class="overlay-wrapper">
                                                    <div class="overlay">
                                                        <div class="overlay-content">
                                                            <div class="content-hidden">
                                                                <p>{{Lang::get('category.shareThisNews')}}</p>
                                                                <ul class="list-inline list-unstyled">
                                                                    <li><a href="#"><i class="fa fa-facebook-square"></i></a>
                                                                    </li>
                                                                    <li><a href="#"><i class="fa fa-twitter-square"></i></a>
                                                                    </li>
                                                                    <li><a href="#"><i class="fa fa-google-plus-square"></i></a>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- .overlay -->

                                                <!-- image -->
                                                <a href="/n/{{$new->id}}">
                                                    <img src="images/news/home/{{$new->image}}" class="img-responsive" title="" alt="" />
                                                </a>
                                                <!-- image -->
                                            </div>
                                            <!-- .media -->

                                            <!-- content -->
                                            <div class="content-wrapper">
                                                <div class="content">

                                                    <!-- cause meta -->
                                                    <div class="meta clearfix">
                                                        <a class="pull-left">Мэдээ мэдээлэл</a>
                                                        <a class="pull-right share-trigger" data-rel="tooltip" title="{{Lang::get('category.shareCause')}}" href="javascript:;"><i class="fa fa-share-alt"></i></a>
                                                    </div>
                                                    <!-- .cause meta -->

                                                    <!-- excerpt -->
                                                    <div class="excerpt">
                                                        <h6>
                                                            <?php
                                                                $buffer_title = $new->title;
                                                                $buffer_title = (strlen($buffer_title) >= 30) ? mb_substr($buffer_title,0,30).'...' : $buffer_title;

                                                                $buffer_title_first = mb_substr($buffer_title,0,1);
                                                                $buffer_title_other = mb_substr($buffer_title,1,strlen($buffer_title));

                                                                $buffer_title_first = mb_strtoupper($buffer_title_first);
                                                                $buffer_title_other = mb_strtolower($buffer_title_other);

                                                                $buffer_title_changed = $buffer_title_first . $buffer_title_other;
                                                                
                                                                $buffer_body = $new->body;
                                                                $buffer_body = (strlen($buffer_body) >= 118) ? mb_substr($buffer_body,0,118).'...' : $buffer_body;

                                                                $converted_date = $new->created_at->format('Y-m-d');
                                                            ?>
                                                        <a href="/n/{{$new->id}}">{{$buffer_title_changed}}</a>
                                                        </h6>
                                                        <p>{{$buffer_body}}</p>
                                                        <span class="grey small">{{$converted_date}} &nbsp; {{Lang::get('category.postedBy')}} (<i>admin</i>)</span>
                                                    </div>
                                                    <!-- .excerpt -->

                                                    <!-- rised slider meta -->
                                                    <div class="slider-meta clearfix">

                                                    </div>
                                                    <!-- .rised slider meta -->


                                                </div>
                                            </div>
                                            <!-- .content -->

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- .1 -->
                            @endforeach

                        </div>
                        @endforeach
                    </div>
                    <!-- .posts -->

                </div>
                <!-- .paginate & posts -->

            </div>
        </div>
        <!-- .blog posts & widgets -->
    </section>
    
@stop