@extends('front._layout.layout')
@section('content')
    
    <!-- breadcrumbs -->
    <section class="breadcrumbs">
        <div class="breadcrumbs-wrapper">
            <div class="container">
                <div class="row">

                    <!-- page title -->
                    <div class="col-md-6 col-xs-6">
                        <h4>{{Lang::get('category.news')}}</h4>
                    </div>
                    <!-- .page title -->

                    <!-- breadcumbs -->
                    <div class="col-md-6 col-xs-6">
                        <ol class="breadcrumb">
                            <li><a href="/">{{Lang::get('category.home')}}</a>
                            </li>
                            <li>
                                <a href="/n">{{Lang::get('category.news')}}</a>
                            </li>
                            <li class="active">
                                <a href="#">-</a>
                            </li>
                        </ol>
                    </div>
                    <!-- .breadcrumbs -->
                </div>
            </div>
        </div>
    </section>
    <!-- .breadcrumbs -->

    <!-- blog posts & widgets -->
    <section class="no-bottom-margin">
        <div class="container">
            <div class="row">

                <!-- paginate & posts -->
                <div class="col-md-9">

                    <!-- post -->
                    <div class="posts">
                        <div class="row">
                            <div class="col-md-12">


                                <div class="box-wrapper">
                                    <div class="box">

                                        <!-- slider -->
                                        <aside>

                                            <div id="singleCarousel" class="carousel slide single-page normal" data-ride="carousel" data-interval="6000">


                                                <!-- Indicators -->
                                                <ol class="carousel-indicators">
                                                	<?php $i = 0; ?>
                                                	@foreach($news->newsImage as $image)
                                                	@if($i == 0)
                                                    <li data-target="#singleCarousel" data-slide-to="{{$i}}" class="active"></li>
                                                    @else
                                                    <li data-target="#singleCarousel" data-slide-to="{{$i}}"></li>
                                                    @endif
                                                    <?php $i = $i+1; ?>
                                                    @endforeach
                                                </ol>

                                                <!-- Wrapper for slides -->
                                                <div class="carousel-inner">
                                                	<?php $i = 0; ?>
                                                	@foreach($news->newsImage as $newsImage)
                                                	@if($i == 0)
                                                    <div class="item active">
                                                        <img src="../images/news/slider/{{$newsImage->Image['path']}}" alt="{{$newsImage->Image['alt']}}" title="" />
                                                    </div>
                                                    @else
                                                    <div class="item">
                                                        <img src="../images/news/slider/{{$newsImage->Image['path']}}" alt="{{$newsImage->Image['alt']}}" title="" />
                                                    </div>
                                                    @endif
                                                    <?php $i = $i+1; ?>
                                                    @endforeach
                                                </div>
                                                <!-- /Wrapper for slides .carousel-inner -->

                                                <!-- Controls -->
                                                <div class="control-box">
                                                    <a class="left carousel-control" href="#singleCarousel" data-slide="prev"><i class="fa fa-angle-left"></i></a>
                                                    <a class="right carousel-control" href="#singleCarousel" data-slide="next"><i class="fa fa-angle-right"></i></a>
                                                </div>
                                                <!-- /.control-box -->


                                            </div>
                                            <!-- carousel -->

                                        </aside>
                                        <!-- .slider -->

                                        <!-- content -->
                                        <div class="content">

                                            <!-- title -->
                                            <h2>
                                            	<?php

                                            		$buffer_title = $news->title;
	                                            	$buffer_title_first = mb_substr($buffer_title,0,1);
		                                            $buffer_title_other = mb_substr($buffer_title,1,strlen($buffer_title));

                                                    $buffer_title_first = mb_strtoupper($buffer_title_first);
		                                            $buffer_title_other = mb_strtolower($buffer_title_other);

		                                            $buffer_title_changed = $buffer_title_first . $buffer_title_other;

                                            	?>
                                            	<a href="#">{{$buffer_title_changed}}</a>
                                            </h2>
                                            <!-- .title -->

                                            <!-- meta -->
                                           <!--  <p>
                                                <span class="grey">published</span> <a href="#">28 July 2014</a>
                                                <span class="grey">by</span> <a href="#">Predrag Stojanovic</a>
                                                <span class="grey">in</span> <a href="#">General</a>
                                                <span class="grey">category with</span>
                                                <a href="#">12</a>
                                                <span class="grey">comments</span>
                                            </p> -->
                                            <!-- .meta -->
                                        </div>
                                        <!-- content -->

                                        <!-- content single -->
                                        <div class="content single">
											{{$news->body}}                                      
                                           <!--  <blockquote>
                                                <p>Temporibus autem quibusdam et aut officiis debitis aut rerum necessitati bus saepe eveniet ut et voluptates repudiandae sint et molestiae.</p>
                                                <footer>
                                                    <cite class="pull-right">by Cicero 3 January 106 BC – 7 December 43 BC</cite>
                                                </footer>
                                            </blockquote> -->
                                        </div>
                                        <!-- .content single -->

                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <!-- .post -->

                </div>
                <!-- .paginate & posts -->

                <!-- sidebar -->
                <div class="col-md-3">

                    <!-- search widget -->
                    <div class="widget">
                        <div class="widget-share">
                            <div class="box-wrapper">
                                <div class="box">

                                    <!-- widget box -->
                                    <div class="widget-box">
                                        <ul class="clearfix">
                                            <li><a href="#" class="facebook"><i class="fa fa-facebook"></i><br /><span class="light-grey">234</span></a>
                                            </li>
                                            <li><a href="#" class="twitter"><i class="fa fa-twitter"></i><br /><span class="light-grey">187</span></a>
                                            </li>
                                            <li><a href="#" class="google-plus"><i class="fa fa-google-plus"></i><br /><span class="light-grey">2.1K</span></a>
                                            </li>
                                        </ul>

                                    </div>
                                    <!-- .widget box -->

                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- .search widget -->

                    <!-- search blog widget -->
                    <div class="widget">
                        <div class="widget-search-causes">
                            <div class="box-wrapper">
                                <div class="box">

                                    <!-- widget title -->
                                    <div class="widget-title">
                                        <h5>{{Lang::get('category.searchNews')}}</h5>
                                    </div>
                                    <!-- .widget title -->

                                    <!-- widget box -->
                                    <div class="widget-box">
                                        <div class="input-group">
                                            <input type="text" class="form-control" placeholder="{{Lang::get('category.typeCauseName')}}" />
                                            <span class="input-group-addon">
                                                <a href="#"><i class="fa fa-search"></i></a>
                                            </span>
                                        </div>
                                    </div>
                                    <!-- .widget box -->

                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- .search blog widget -->

                    <!-- archives widget -->
                    <div class="widget">
                        <div class="widget-search-causes">
                            <div class="box-wrapper">
                                <div class="box">

                                    <!-- widget title -->
                                    <div class="widget-title">
                                        <h5>{{Lang::get('category.causes')}}</h5>
                                    </div>
                                    <!-- .widget title -->
                                    @foreach($posts as $post)
                                    <!-- widget box -->
                                    <div class="widget-box">
                                        <div class="media">
                                            <a href="../p/{{$post->id}}">
                                                <img src="{{$post->image}}" class="img-responsive" title="" alt="" />
                                            </a>
                                        </div>
                                    </div>
                                    <!-- .widget box -->

                                    <!-- widget box -->
                                    <div class="widget-box">
                                    	<?php
                                            $buffer_title = $post->title;
                                            $buffer_title = (strlen($buffer_title) >= 50) ? mb_substr($buffer_title,0,50).'...' : $buffer_title;

                                            $buffer_title_first = mb_substr($buffer_title,0,1);
                                            $buffer_title_other = mb_substr($buffer_title,1,strlen($buffer_title));

                                            $buffer_title_other = mb_strtolower($buffer_title_other);

                                            $buffer_title_changed = $buffer_title_first . $buffer_title_other;
                                        ?>
                                        <p><a href="../p/{{$post->id}}">{{$buffer_title_changed}}</a>
                                            <br />
                                            <span class="grey"><a href="#">Төрөл: {{$post->category}}</a>
                                            </span>
                                    </div>
                                    <!-- .widget box -->

                                    <hr />
                                    @endforeach

                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- .archives widget -->

                </div>
                <!-- .sidebar -->

            </div>
        </div>
        <!-- .blog posts & widgets -->

    </section>

    @if( !empty($newsSimilar) )
    <!-- similar posts -->
    <section class="no-top-margin">
        <div class="container">
            <div class="row">
                <div class="col-md-12">

                    <!-- heading -->
                    <div class="heading">

                        <!-- title -->
                        <h3>{{Lang::get('category.similarNews')}}</h3>
                        <!-- .title -->

                        <div class="border">
                            <div class="border-inner">
                            </div>
                        </div>
                        <!-- .carousel slide -->

                    </div>
                    <!-- .heading -->

                </div>
            </div>

            <!-- posts -->
            <div class="posts">
                <div class="row">

                	@foreach($newsSimilar as $similar)
                    <!-- 1 -->
                    <div class="col-md-3">
                        <div class="box-wrapper">
                            <div class="box">
                                <div class="latest-box">

                                    <!-- .media -->
                                    <div class="media">

                                        <!-- overlay -->
                                        <div class="overlay-wrapper">
                                            <div class="overlay">
                                                <div class="overlay-content">
                                                    <div class="content-hidden">
                                                        <p>{{Lang::get('category.shareThisNews')}}</p>
                                                        <ul class="list-inline list-unstyled">
                                                            <li><a href="#"><i class="fa fa-facebook-square"></i></a>
                                                            </li>
                                                            <li><a href="#"><i class="fa fa-twitter-square"></i></a>
                                                            </li>
                                                            <li><a href="#"><i class="fa fa-google-plus-square"></i></a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- .overlay -->

                                        <!-- image -->
                                        <a href="/n/{{$similar->id}}">
                                            <img src="../images/news/home/{{$similar->image}}" class="img-responsive" title="" alt="" />
                                        </a>
                                        <!-- image -->
                                    </div>
                                    <!-- .media -->

                                    <!-- content -->
                                    <div class="content-wrapper">
                                        <div class="content">

                                            <!-- cause meta -->
                                            <div class="meta clearfix">
                                                <a class="pull-left" href="#">Tag эсвэл Category</a>
                                                <a class="pull-right share-trigger" data-rel="tooltip" title="{{Lang::get('category.shareCause')}}" href="javascript:;"><i class="fa fa-share-alt"></i></a>
                                            </div>
                                            <!-- .cause meta -->

                                            <!-- excerpt -->
                                            <div class="excerpt">
                                                <h6>
                                                	<?php
			                                            $buffer_title = $similar->title;
			                                            $buffer_title = (strlen($buffer_title) >= 40) ? mb_substr($buffer_title,0,40).'...' : $buffer_title;

			                                            $buffer_title_first = mb_substr($buffer_title,0,1);
			                                            $buffer_title_other = mb_substr($buffer_title,1,strlen($buffer_title));

			                                            $buffer_title_other = mb_strtolower($buffer_title_other);

			                                            $buffer_title_changed = $buffer_title_first . $buffer_title_other;
			                                        ?>
                                                	<a href="/n/{{$similar->id}}">{{$buffer_title_changed}}</a>
                                                </h6>

                                            </div>
                                            <!-- .excerpt -->

                                            <!-- rised slider meta -->
                                            <div class="slider-meta clearfix">

                                            </div>
                                            <!-- .rised slider meta -->

                                        </div>
                                    </div>
                                    <!-- .content -->

                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- .1 -->
                    @endforeach

                </div>
            </div>
            <!-- .posts -->

        </div>
    </section>
    <!-- similar posts -->
    @endif
@stop