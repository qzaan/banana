@extends('front._layout.layout')
@section('content')

    <!-- breadcrumbs -->
    <section class="breadcrumbs">
        <div class="breadcrumbs-wrapper">
            <div class="container">
                <div class="row">

                    <!-- page title -->
                    <div class="col-md-6 col-xs-6">
                        <h4>{{Lang::get('category.cause')}}</h4>
                    </div>
                    <!-- .page title -->

                    <!-- breadcumbs -->
                    <div class="col-md-6 col-xs-6">
                        <ol class="breadcrumb">
                            <li><a href="/">{{Lang::get('category.home')}}</a>
                            </li>
                            <li>
                                <a href="/p">{{Lang::get('category.cause')}}</a>
                            </li>
                            <li class="active">
                                <a href="#">-</a>
                            </li>
                        </ol>
                    </div>
                    <!-- .breadcrumbs -->
                </div>
            </div>
        </div>
    </section>
    <!-- .breadcrumbs -->

    <!-- blog posts & widgets -->
    <section class="no-bottom-margin">
        <div class="container">
            <div class="row">

                <!-- paginate & posts -->
                <div class="col-md-9">

                    <!-- post -->
                    <div class="posts">
                        <div class="row">
                            <div class="col-md-12">


                                <div class="box-wrapper">
                                    <div class="box">

                                        <!-- slider -->
                                        <aside>

                                            <div id="singleCarousel" class="carousel slide single-page normal" data-ride="carousel" data-interval="false">


                                                <!-- Indicators -->
                                                <ol class="carousel-indicators">
                                                    <?php $i = 0; ?>
                                                	@foreach($post->postImage as $image)
                                                	@if($i == 0)
                                                    <li data-target="#singleCarousel" data-slide-to="{{$i}}" class="active"></li>
                                                    @else
                                                    <li data-target="#singleCarousel" data-slide-to="{{$i}}"></li>
                                                    @endif
                                                    <?php $i = $i+1; ?>
                                                    @endforeach
                                                </ol>


                                                <!-- Wrapper for slides -->
                                                <div class="carousel-inner">
                                                	<?php $i = 0; ?>
                                                	@foreach($post->postImage as $postImage)
                                                	@if($i == 0)
                                                    <div class="item active">
                                                        <img src="../images/posts/slider/{{$postImage->Image['path']}}" alt="{{$postImage->Image['alt']}}" title="" />
                                                    </div>
                                                    @else
                                                    <div class="item">
                                                        <img src="../images/posts/slider/{{$postImage->Image['path']}}" alt="{{$postImage->Image['alt']}}" title="" />
                                                    </div>
                                                    @endif
                                                    <?php $i = $i+1; ?>
                                                    @endforeach
                                                </div>
                                                <!-- /Wrapper for slides .carousel-inner -->

                                                <!-- Controls -->
                                                <div class="control-box">
                                                    <a class="left carousel-control" href="#singleCarousel" data-slide="prev"><i class="fa fa-angle-left"></i></a>
                                                    <a class="right carousel-control" href="#singleCarousel" data-slide="next"><i class="fa fa-angle-right"></i></a>
                                                </div>
                                                <!-- /.control-box -->


                                            </div>
                                            <!-- carousel -->

                                        </aside>
                                        <!-- .slider -->

                                        <!-- content -->
                                        <div class="content comments clearfix">

                                            <!-- rised bar -->
                                            <div class="slider-content cause">
                                                <input class="rised" data-slider-id='ex1Slider' type="text" data-slider-min="0" data-slider-max="{{$post->rec}}" data-slider-step="1" data-slider-enabled="false" data-slider-value="{{$post->cur}}" />
                                            </div>
                                            <!-- .rised bar -->

                                            <!-- rised slider meta -->
                                            <div class="clearfix">
                                                <p class="pull-left lead">
                                                    <span class="light-grey">{{Lang::get('category.raised');}}</span>
                                                    ₮<?php echo number_format($post->cur) ?>
                                                </p>
                                                <p class="pull-right lead">
                                                    <span class="light-grey">{{Lang::get('category.goal');}}</span>
                                                    ₮<?php echo number_format($post->rec) ?>
                                                </p>
                                            </div>
                                            <!-- .rised slider meta -->

                                        </div>
                                        <!-- content -->

                                        <!-- border -->
                                        <hr class="inline-hr" />
                                        <!-- .border -->

                                        <!-- content -->
                                        <div class="content">

                                            <!-- title -->
                                            <h2>
                                                <?php
                                                    $buffer_title = $post->title;
                                                    $buffer_title_first = mb_substr($buffer_title,0,1);
                                                    $buffer_title_other = mb_substr($buffer_title,1,strlen($buffer_title));

                                                    $buffer_title_first = mb_strtoupper($buffer_title_first);
                                                    $buffer_title_other = mb_strtolower($buffer_title_other);

                                                    $buffer_title_changed = $buffer_title_first . $buffer_title_other;
                                                ?>
                                                <a>{{$buffer_title_changed}}</a>
                                            </h2>
                                            <!-- .title -->

                                        </div>
                                        <!-- content -->

                                        <!-- content single -->
                                        <div class="content single">
                                            <p>
                                                {{$post->body}}
                                            </p>
                                            
                                            @if($post->url != "")
                                            <!-- Video -->
                                            <div class="col-md-4 col-xs-6 " style="float:right">
                                                <div class="box-wrapper" title="{{Lang::get('category.desc_Video')}}">
                                                    <div class="box">
                                                        <div class="media">
                                                            <div class="play">
                                                                <img id="Pplayer" src="../images/play.png" /> 
                                                            </div>
                                                            <a href="http://youtube.com" 
                                                               data-remote="{{$post->url}}" 
                                                               data-title="{{Lang::get('category.desc_Video')}}" 
                                                               data-toggle="lightbox" 
                                                               data-gallery="multiimages">
                                                               <img src="../images/posts/home/{{$post->image}}" title="" alt="" />
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- Video -->
                                            @endif

                                            <blockquote>
                                                <p>{{Lang::get('category.testText6')}}</p>
                                                <footer>
                                                    <cite class="pull-right">{{Lang::get('category.enddate')}} <b>{{$post->end_date}}</b> -нд</cite>
                                                </footer>
                                            </blockquote>
                                        </div>
                                        <!-- .content single -->

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- .post -->

                    <!-- comments -->
                    <div class="posts">
                        <div class="row">
                            <div class="col-md-12">

                                <div class="box-wrapper">
                                    <div class="box">

                                        <!-- content -->
                                        <div class="content comments clearfix">

                                            <!-- title -->
                                            <h5 class="pull-left">{{Lang::get('category.recentComments')}}</h5>
                                            <!-- .title -->

                                            <!-- title -->
                                            <span id="comment_count">
                                                <p class="pull-right grey">{{$comment_count}} {{Lang::get('category.comments')}}</p>
                                            </span>
                                            <!-- .title -->

                                        </div>
                                        <!-- content -->

                                        <hr class="inline-hr" />

                                        <div id="commentShow">
                                            @foreach($comments as $comment)
                                            <!-- 1 -->
                                            <div class="content comments">

                                                <div class="media">

                                                    <div class="small-product pull-left">
                                                        <div class="small-product-wrapper">                                                            
                                                            <img class="media-object img-thumbnail img-circle" src="../images/user/userComment.png" title="" alt="" />
                                                        </div>
                                                    </div>

                                                    <div class="media-body small-product">
                                                        <p class="lead">
                                                            <span class="grey">
                                                                <!-- <a href="#"> -->
                                                                    {{$comment->title}}
                                                                <!-- </a> -->
                                                            </span> 
                                                        </p>
                                                        <p>{{$comment->body}}</p>
                                                        <span class="grey">
                                                            <i class="fa fa-clock-o"></i>
                                                            <?php

                                                                $now = new DateTime();
                                                                $old = new DateTime( $comment->created_at );
                                                                $interval = $now->diff($old);

                                                                $year = $interval->format('%y');
                                                                $month = $interval->format('%m');
                                                                $day = $interval->format('%d');
                                                                $hour = $interval->format('%h');
                                                                $minute = $interval->format('%i');  

                                                                $new = 1;
                                                                if($year != 0){ $new = 0; echo $year . " "; ?>{{Lang::get('category.year')}}<?php }
                                                                if($month != 0){ $new = 0; echo " " . $month . " "; ?>{{Lang::get('category.month')}}<?php }
                                                                if($day != 0){ $new = 0; echo " " . $day . " "; ?>{{Lang::get('category.day')}}<?php }
                                                                if($hour != 0){ $new = 0; echo " " . $hour . " "; ?>{{Lang::get('category.hour')}}<?php }
                                                                if($minute != 0){ $new = 0; echo " " . $minute . " "; ?>{{Lang::get('category.minute')}}<?php }

                                                                if($new == 1){ echo " 0 "; ?>{{Lang::get('category.minute')}}<?php }
                                                            ?>

                                                            {{Lang::get('category.ago')}}
                                                        </span>
                                                    </div>
                                                </div>

                                            </div>

                                            <hr />
                                            <!-- .1 -->
                                            @endforeach
                                        </div>

                                        <!-- tags & prev next -->
                                        <div class="content">
                                            <div class="row">

                                                <!-- paginate posts -->
                                                <div class="col-md-12">
                                                    <div class="paginate single pull-right">
                                                        <ul class="list-unstyled list-inline showmore">
                                                            <input type="hidden" name="hiddenPostID" id="hiddenPostID" value="{{$post->id}}" />
                                                            @if($comments != null)
                                                            <a id="readmore" href="#">{{Lang::get('category.readmore')}}</a>
                                                            @endif
                                                        </ul>
                                                    </div>
                                                </div>
                                                <!-- .paginate posts -->

                                            </div>
                                        </div>
                                        <!-- .tags & prev next -->

                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <!-- .comments -->

                    <!-- leave comment -->
                    <div class="posts">
                        <div class="row">

                            <div class="col-md-12">


                                <div class="box-wrapper">
                                    <div class="box">


                                        <!-- content -->
                                        <div class="content comments clearfix">

                                            <!-- title -->
                                            <h5 class="pull-left">{{Lang::get('category.leaveComment')}}</h5>
                                            <!-- .title -->

                                        </div>
                                        <!-- content -->

                                        <hr class="inline-hr" />

                                        <!-- form -->
                                        <div class="content comments clearfix">
                                            {{Form::open( array('method' => 'post','id'=>'postcomment','enctype' => 'multipart/form-data') )}}
                                                <!-- postID -->
                                                <input name="_postID" type="hidden" value="{{$post->id}}"/>

                                                <!-- widget box -->
                                                <div class="widget-box">
                                                <label>{{Lang::get('category.yourMail')}}</label>
                                                @if(Sentry::check())
                                                    <input id="cMail" class="form-control" name="cMail" value="{{Sentry::getUser()->email}}" readOnly="true"></input>
                                                @else
                                                    <input id="cMail" class="form-control" name="cMail"></input>
                                                @endif
                                                </div>
                                                <!-- .widget box -->

                                                <!-- widget box -->
                                                <div class="widget-box">
                                                    <label>{{Lang::get('category.yourTitle')}}</label>
                                                    <input id="cTitle" class="form-control" name="cTitle"></input>
                                                </div>
                                                <!-- .widget box -->

                                                <!-- widget box -->
                                                <div class="widget-box">
                                                    <label>{{Lang::get('category.yourBody')}}</label>
                                                    <textarea id="cBody" class="form-control" name="cBody"></textarea>
                                                </div>
                                                <!-- .widget box -->

                                                <div id="success"></div>
                                                <div id="error"></div>
                                                
                                                <!-- widget box -->
                                                <div class="widget-box">
                                                    {{Form::submit(Lang::get('category.leaveComment'), array('class' => 'button-normal white pull-right','id' => 'pForm'))}}
                                                </div>
                                                <!-- .widget-box -->
                                            {{Form::token()}}
                                            {{Form::close()}}   
                                        </div>
                                        <!-- .form -->

                                        <hr />
                                        <!-- .leave comments -->

                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <!-- .comments -->

                </div>
                <!-- .paginate & posts -->

                <!-- sidebar -->
                <div class="col-md-3">

                    <!-- author widget -->
                    <div class="widget">
                        <div class="widget-author">
                            <div class="box-wrapper">
                                <div class="box">

                                    <!-- widget box -->
                                    <div class="widget-box text-center">
                                        <div class="media author">
                                            <div class="small-product">
                                                <div class="small-product-wrapper">
                                                    <a href="javascript:void(0)">
                                                        <?php 
                                                            $url = "noImage.png";
                                                            if( $post->user->request['image'] != "" ) 
                                                            {
                                                                $url = $post->user->request['image'];
                                                            }
                                                        ?>
                                                        <img class="media-object img-circle img-thumbnail " src="/images/user/{{$url}}" title="" alt="" />
                                                    </a>
                                                </div>
                                                <div class="media-body small-product">
                                                    <p>
                                                        <span class="light-grey">{{Lang::Get('category.addedBy')}}</span>
                                                    </p>
                                                    <p class="lead">
                                                        <a>{{$post->user['first_name']}} {{$post->user['last_name']}}</a> 
                                                    </p>
                                                    <p>
                                                        <span class="grey"><?php echo $post->created_at->format('Y-m-d'); ?> -нд</span>
                                                    </p>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                    <!-- .widget box -->

                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- .author widget -->

                    <!-- share widget -->
                    <div class="widget">
                        <div class="widget-share">
                            <div class="box-wrapper">
                                <div class="box">

                                    <!-- widget box -->
                                    <div class="widget-box">
                                        <ul class="clearfix">
                                            <li><a href="#" class="facebook"><i class="fa fa-facebook"></i><br /><span class="light-grey">234</span></a>
                                            </li>
                                            <li><a href="#" class="twitter"><i class="fa fa-twitter"></i><br /><span class="light-grey">187</span></a>
                                            </li>
                                            <li><a href="#" class="google-plus"><i class="fa fa-google-plus"></i><br /><span class="light-grey">2.1K</span></a>
                                            </li>
                                        </ul>

                                    </div>
                                    <!-- .widget box -->

                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- .share widget -->

                    <!-- to go widget -->
                    <div class="widget">
                        <div class="widget-cause">
                            <div class="box-wrapper">
                                <div class="box">

                                    <!-- widget box -->
                                    <?php
                                        $togo = $post->rec - $post->cur;
                                    ?>
                                    <h4 class="text-center">₮<?php echo number_format($togo) ?>
                                        <span class="light-grey">{{Lang::get('category.togo')}}</span>
                                    </h4>
                                    <!-- .widget box -->

                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- .to go widget -->

                    <!-- donate widget -->
                    <div class="widget">
                        <div class="widget-donate">
                            <div class="box-wrapper">
                                <div class="box">

                                    <!-- widget box -->
                                    <h4 class="text-center"><a href="/d/{{$post->id}}">{{Lang::get('category.donateUpper')}}</a>
                                    </h4>
                                    <!-- .widget box -->

                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- .donate widget -->

                </div>
                <!-- .sidebar -->

            </div>
        </div>
        <!-- .blog posts & widgets -->

    </section>

@stop