@extends('front._layout.layout')
@section('content')

    <!-- breadcrumbs -->
    <section class="breadcrumbs">
        <div class="breadcrumbs-wrapper">
            <div class="container">
                <div class="row">

                    <!-- page title -->
                    <div class="col-md-6 col-xs-6">
                        <h4>{{Lang::get('category.cause')}}</h4>
                    </div>
                    <!-- .page title -->

                    <!-- breadcumbs -->
                    <div class="col-md-6 col-xs-6">
                        <ol class="breadcrumb">
                            <li><a href="/">{{Lang::get('category.home')}}</a>
                            </li>
                            <li class="active">
                                <a href="/p">{{Lang::get('category.cause')}}</a>
                            </li>
                        </ol>
                    </div>
                    <!-- .breadcrumbs -->
                </div>
            </div>
        </div>
    </section>
    <!-- .breadcrumbs -->

    <!-- blog posts & widgets -->
    <section>
        <div class="container">
            <div class="row">

                <!-- paginate & posts -->
                <div class="col-md-9">

                    <?php echo $posts->links(); ?>

                    <!-- posts -->
                    <div class="posts" id="posts">
                        @foreach(array_chunk($posts->getCollection()->all(),2) as $row)
                        <div class="row">
                            @foreach($row as $post)
                            <div class="col-md-6">
                                <div class="box-wrapper">
                                    <div class="box">
                                        <div class="latest-box">

                                            <!-- .media -->
                                            <div class="media">

                                                <!-- overlay -->
                                                <div class="overlay-wrapper">
                                                    <div class="overlay">
                                                        <div class="overlay-content">
                                                            <div class="content-hidden">
                                                                <p>{{Lang::get('category.shareThisCause')}}</p>
                                                                <ul class="list-inline list-unstyled">
                                                                    <li><a href="#"><i class="fa fa-facebook-square"></i></a>
                                                                    </li>
                                                                    <li><a href="#"><i class="fa fa-twitter-square"></i></a>
                                                                    </li>
                                                                    <li><a href="#"><i class="fa fa-google-plus-square"></i></a>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- .overlay -->

                                                <!-- image -->
                                                <a href="/p/{{$post->id}}">
                                                    <img src="images/posts/home/{{$post->image}}" class="img-responsive" title="" alt="" />
                                                </a>
                                                <!-- image -->
                                            </div>
                                            <!-- .media -->

                                            <!-- content -->
                                            <div class="content-wrapper">
                                                <div class="content">

                                                    <!-- cause meta -->
                                                    <div class="meta clearfix">
                                                        <a class="pull-left">{{$post->category['title']}}</a>
                                                        <a class="pull-right share-trigger" data-rel="tooltip" title="{{Lang::get('category.shareCause')}}" href="javascript:void(0);"><i class="fa fa-share-alt"></i></a>
                                                    </div>
                                                    <!-- .cause meta -->

                                                    <!-- excerpt -->
                                                    <div class="excerpt">
                                                        <h6>
                                                            <?php
                                                                $buffer_title = $post->title;
                                                                $buffer_title = (strlen($buffer_title) >= 64) ? mb_substr($buffer_title,0,64).'...' : $buffer_title;

                                                                $buffer_title_first = mb_substr($buffer_title,0,1);
                                                                $buffer_title_other = mb_substr($buffer_title,1,strlen($buffer_title));

                                                                $buffer_title_first = mb_strtoupper($buffer_title_first);
                                                                $buffer_title_other = mb_strtolower($buffer_title_other);

                                                                $buffer_title_changed = $buffer_title_first . $buffer_title_other;
                                                            ?>
                                                            <a href="/p/{{$post->id}}">{{$buffer_title_changed}}</a>
                                                        </h6>

                                                    </div>
                                                    <!-- .excerpt -->

                                                    <!-- rised bar -->
                                                    <div class="slider-content">
                                                        <input class="rised" data-slider-id='ex1Slider' type="text" data-slider-min="0" data-slider-max="{{$post->rec}}" data-slider-step="1" data-slider-enabled="false" data-slider-value="{{$post->cur}}" />
                                                    </div>
                                                    <!-- .rised bar -->

                                                    <!-- rised slider meta -->
                                                    <div class="slider-meta clearfix">
                                                        <span class="pull-left">₮<?php echo number_format($post->cur) ?></span>
                                                        <span class="pull-right">₮<?php echo number_format($post->rec) ?></span>
                                                    </div>
                                                    <!-- .rised slider meta -->


                                                </div>
                                            </div>
                                            <!-- .content -->

                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endforeach

                        </div>
                        @endforeach
                    </div>
                    <!-- .posts -->

                </div>
                <!-- .paginate & posts -->

                <!-- sidebar -->
                @include('front._layout.search')

            </div>
        </div>
    </section>
    <!-- .blog posts & widgets -->
@stop