<?php $noPagiTotal = 0; ?>

@foreach( $arr_cache_all as $cache )
    <!-- widget box -->
    <div class="widget-box">

        <div class="media">

            <div class="small-product pull-left">
                <div class="small-product-wrapper">
                    <img height="60px" width="60px" class="media-object img-thumbnail img-circle scart" src="{{$cache['url']}}" title="" alt="" />
                </div>
            </div>

            <div class="media-body small-product">
                <h6 class="media-heading">
                    <?php
                        $buffer_title = $cache['title'];
                        $buffer_title = (strlen($buffer_title) >= 20) ? mb_substr($buffer_title,0,20).'...' : $buffer_title;
                        $buffer_title_first = mb_substr($buffer_title,0,1);
                        $buffer_title_other = mb_substr($buffer_title,1,strlen($buffer_title));

                        $buffer_title_first = mb_strtoupper($buffer_title_first);
                        $buffer_title_other = mb_strtolower($buffer_title_other);

                        $buffer_title_changed = $buffer_title_first . $buffer_title_other;
                    ?>
                    <a>{{$buffer_title_changed}}</a>

                    <!-- Remove button -->
                    <?php
                        $onId = $cache['id'];
                        $onSize = $cache['size'];
                    ?>
                    <a onclick="popProduct({{$onId}},'{{$onSize}}')" href="javascript:void(0)"><i class="fa fa-times pull-right"></i></a>
                </h6>
                <p>
                    <span id="pro_count">{{$cache['count']}}</span>ш - 
                    <span id="pro_size">{{$cache['size']}}</span>
                </p>
            </div>
        </div>

    </div>
    <!-- .widget box -->

    <!-- widget box -->
    <div class="widget-box">
        <div class="sub-total">
            <p class="text-right">{{Lang::get('category.subtotal')}}:
                <?php
                    $price = $cache['price'] * $cache['count'];
                    $noPagiTotal += $price;
                ?>
                <span class="pro_price">₮{{$price}}</span>
                <input class="hiddenTotal" type="hidden" value="{{$total}}" />
            </p>
        </div>
    </div>
    <!-- .widget box -->
@endforeach

@if( !$arr_cache_all->isEmpty() )
    <div id="shopPagination">
        {{ $arr_cache_all->links() }}
    </div>
@endif

        


