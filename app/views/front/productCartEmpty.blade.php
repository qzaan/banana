<!-- widget box -->
<div class="widget-box">

    <div class="media">

        <div class="small-product pull-left">
            <div class="small-product-wrapper">
                <a href="javascript:void(0)">
                    <img class="media-object img-thumbnail img-circle" src="../images/empty_product.png" title="" alt="" />
                </a>
            </div>
        </div>

        <div class="media-body small-product">
            <h6 class="media-heading">
                <a href="javascript:void(0)">{{Lang::get('category.empty')}}</a>
            </h6>
            <p>
                <span id="pro_count">0</span> X 
                <span id="pro_size">0</span>
            </p>
        </div>
    </div>

</div>
<!-- .widget box -->

<!-- widget box -->
<div class="widget-box">
    <div class="sub-total">
        <p class="text-right">{{Lang::get('category.subtotal')}}:
            <span class="pro_price">₮0</span>
        </p>
    </div>
</div>
<!-- .widget box -->