@extends('front._layout.layout')
@section('content')

    <!-- breadcrumbs -->
    <section class="breadcrumbs">
        <div class="breadcrumbs-wrapper">
            <div class="container">
                <div class="row">

                    <!-- page title -->
                    <div class="col-md-6 col-xs-6">
                        <h4>{{Lang::get('category.requestDonation');}}</h4>
                    </div>
                    <!-- .page title -->

                    <!-- breadcumbs -->
                    <div class="col-md-6 col-xs-6">
                        <ol class="breadcrumb">
                            <li><a href="#">{{Lang::get('category.home')}}</a>
                            </li>
                            <li class="active"><a href="#">-</a>
                            </li>
                        </ol>
                    </div>
                    <!-- .breadcrumbs -->
                </div>
            </div>
        </div>
    </section>
    <!-- .breadcrumbs -->   
    @if(Sentry::check())
    <!-- contact form & additional info -->
    <section>
        <div class="container">
            <div class="row">

                <div class="col-md-8 col-md-offset-2">

                    <div class="widget">
                        <div class="box-wrapper">
                            <div class="box">

                                <!-- heading -->
                                <div class="form-heading">
                                    <h5>{{Lang::get('category.form')}}</h5>
                                </div>
                                <!-- heading -->

                                <!-- border -->
                                <hr />
                                <!-- .border -->

                                <!-- form -->
                                <div class="content">
                                    {{Form::open( array('method' => 'post','id'=>'rqstDonate','enctype' => 'multipart/form-data') )}}

                                        <!-- widget box -->
                                        <div class="widget-box">
                                            <div class="form-group">
                                                <label>{{Lang::get('category.yourPhone')}}</label>
                                                <input id="rqstPhone" name="rqstPhone" placeholder="{{Lang::get('category.desc_Phone')}}" type="text" class="form-control" />
                                            </div>
                                        </div>
                                        <!-- .widget box -->

                                        <!-- widget box -->
                                        <div class="widget-box">
                                            <div class="form-group">
                                                <label>{{Lang::get('category.yourMail')}}</label>
                                                <input id="rqstMail" name="rqstMail" placeholder="{{Lang::get('category.desc_Email')}}" type="text" class="form-control" />
                                            </div>
                                        </div>
                                        <!-- .widget box -->

                                        <!-- widget box -->
                                        <div class="widget-box">
                                            <div class="form-group">
                                                <label>{{Lang::get('category.videoURL')}}</label>
                                                <input id="rqstURL" name="rqstURL" placeholder="{{Lang::get('category.desc_Url')}}" type="text" class="form-control" />
                                            </div>
                                        </div>
                                        <!-- .widget box -->

                                        <!-- widget box -->
                                        <div class="widget-box">
                                            <div class="form-group">
                                                <label>{{Lang::get('category.image')}}</label>    
                                                <a href="javascript:void(0)" onclick="choosePicture()"><u>{{Lang::get('category.upload')}}</u></a href="javascript:void(0)">    
                                                <input type="file" id="choose" name="rqstImage" style="visibility: hidden;"/>
                                                <img id="profileImageResized" src="../images/user/noImage.png">
                                            </div>

                                            <!-- Modal -->
                                            <div id="profileModal" class="modal fade bs-example-modal-lg" role="dialog" aria-labelledby="gridSystemModalLabel">
                                                <div class="modal-dialog" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button id="profileCloseModal" type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                            <h4 class="modal-title" id="gridSystemModalLabel">Хэмжээ тааруулах</h4>
                                                        </div>
                                                        <div class="modal-bodyCrop">
                                                            <!-- Crop size -->
                                                            <div class="overlayProfile">
                                                                <div class="overlayProfile-inner">
                                                                </div>
                                                            </div>
                                                            <!-- .Crop size -->
                                                            <img class="resize-image" id="profileImageResizing" src="" alt="image for resizing">
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-primary js-crop">Тайрах<img class="icon-crop" src="images/crop.svg"></button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- .Modal -->
                                        </div>
                                        <!-- .widget box -->

                                        <!-- widget box -->
                                        <div class="widget-box">
                                            <div class="form-group">
                                                <label>{{Lang::get('category.yourTitle')}}</label>
                                                <input id="rqstTitle" name="rqstTitle" placeholder="" type="text" class="form-control" />
                                            </div>
                                        </div>
                                        <!-- .widget box -->

                                        <!-- widget box -->
                                        <div class="widget-box">
                                            <div class="form-group">
                                                <label>{{Lang::get('category.yourBody')}}</label>
                                                <textarea id="rqstBody" name="rqstBody" placeholder="" class="form-control"></textarea>
                                            </div>
                                        </div>
                                        <!-- .widget box -->

                                        <div id="error"></div>

                                        <!-- widget box -->
                                        <div class="widget-box">
                                            <div class="form-group">
                                                <input name="_userID" type="hidden" value="{{Sentry::getUser()->id}}"/>
                                                {{Form::submit(Lang::get('category.send'), array('class' => 'button-normal full white','id' => 'rqstForm'))}}
                                            </div>
                                        </div>
                                        <!-- .widget-box -->

                                    {{Form::token()}}
                                    {{Form::close()}} 
                                </div>
                                <!-- .form -->

                            </div>
                        </div>
                    </div>
                    

                </div>



            </div>
        </div>
    </section>
    @endif
@stop