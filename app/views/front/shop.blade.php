@extends('front._layout.layout')
@section('content')

    <!-- breadcrumbs -->
    <section class="breadcrumbs">
        <div class="breadcrumbs-wrapper">
            <div class="container">
                <div class="row">

                    <!-- page title -->
                    <div class="col-md-6 col-xs-6">
                        <h4>{{Lang::get('category.shop')}}</h4>
                    </div>
                    <!-- .page title -->

                    <!-- breadcumbs -->
                    <div class="col-md-6 col-xs-6">
                        <ol class="breadcrumb">
                            <li><a href="/">{{Lang::get('category.home')}}</a>
                            </li>
                            <li class="active"><a href="#">{{Lang::get('category.shop')}}</a>
                            </li>
                        </ol>
                    </div>
                    <!-- .breadcrumbs -->
                </div>
            </div>
        </div>
    </section>
    <!-- .breadcrumbs -->

    <!-- blog posts & widgets -->
    <section>
        <div class="container">
            <div class="row">

                <!-- paginate & posts -->
                <div class="col-md-8">

                    <?php echo $products->links(); ?>

                    <!-- posts -->
                    <div class="posts">
                        @foreach(array_chunk($products->getCollection()->all(),2) as $row)
                        <div class="row">
                            @foreach($row as $product)
                            <!-- 1 -->  
                            <div class="col-md-6">
                                <div class="box-wrapper">
                                    <div class="box">
                                        <div class="latest-box">

                                            <!-- content -->
                                            <div class="content-wrapper">
                                                <div class="content">

                                                    <!-- cause meta -->
                                                    <div class="meta clearfix">
                                                        <a class="pull-left">₮<?php echo number_format($product->price) ?></a>
                                                        
                                                        <!-- hidden values -->
                                                        <h4 id="proPrice" class="hidden">₮{{$product->price}}</h4>
                                                        <input id="proId" type="hidden" value="{{$product->id}}" />
                                                        <span id="pick" class="hidden">{{$product->size}}</span>
                                                        <h4 id="proTitle" class="hidden">{{$product->title}}</h4>
                                                        <div class="increment hidden">
                                                            <input type="hidden" class="form-control" value="1" readonly>
                                                        </div>
                                                        <!-- .hidden values -->

                                                        <a onclick="pushCart()" class="pull-right cart" data-rel="tooltip" title="{{Lang::get('category.addToCart')}}" href="javascript:void(0)"><i class="fa fa-shopping-cart"></i></a>
                                                    </div>
                                                    <!-- .cause meta -->

                                                    <!-- excerpt -->
                                                    <div class="excerpt">
                                                        <h6>
                                                            <?php
                                                                $buffer_title = $product->title;
                                                                $buffer_title = (strlen($buffer_title) >= 32) ? mb_substr($buffer_title,0,32).'...' : $buffer_title;
                                                                $buffer_title_first = mb_substr($buffer_title,0,1);
                                                                $buffer_title_other = mb_substr($buffer_title,1,strlen($buffer_title));

                                                                $buffer_title_first = mb_strtoupper($buffer_title_first);
                                                                $buffer_title_other = mb_strtolower($buffer_title_other);

                                                                $buffer_title_changed = $buffer_title_first . $buffer_title_other;
                                                            ?>
                                                            <a href="/s/{{$product->id}}">{{$buffer_title_changed}}</a>
                                                        </h6>
                                                    </div>
                                                    <!-- .excerpt -->

                                                </div>
                                            </div>
                                            <!-- .content -->

                                            <!-- .media -->
                                            <div class="media">

                                                <!-- image -->
                                                <a href="/s/{{$product->id}}" class="shop">
                                                    <div class="sale">{{Lang::get('category.sale')}}</div>
                                                    <img id="image" src="{{$product->image}}" class="img-responsive" title="" alt="" />
                                                </a>
                                                <!-- image -->

                                            </div>
                                            <!-- .media -->

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- .1 -->
                            @endforeach
                        </div>
                        @endforeach
                    </div>
                    <!-- .posts -->

                </div>
                <!-- .paginate & posts -->

                <!-- sidebar -->
                <div class="col-md-4">

                    <!-- categories widget -->
                    <div class="widget">
                        <div class="widget-search-causes">
                            <div class="box-wrapper">
                                <div class="box">

                                    <!-- widget title -->
                                    <div class="widget-title">
                                        <h5>{{Lang::get('category.shopCategories')}}</h5>
                                    </div>
                                    <!-- .widget title -->

                                    <!-- widget box -->
                                    <div class="widget-box">
                                        <ul class="list-unstyled">
                                            <li>
                                                <a href="#">
                                                    Бүгд
                                                    <i class="fa fa-angle-right pull-right"></i>
                                                </a>
                                            </li>
                                            @foreach($categories as $category)
                                            <li>
                                                <a href="#">
                                                    {{$category->categoryName}}
                                                    <i class="fa fa-angle-right pull-right"></i>
                                                </a>
                                            </li>
                                            @endforeach
                                        </ul>
                                    </div>
                                    <!-- .widget box -->

                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- .categories widget -->


                    <!-- price filter widget -->
                    <div class="widget">
                        <div class="widget-search-causes">
                            <div class="box-wrapper">
                                <div class="box">

                                    <!-- widget title -->
                                    <div class="widget-title">
                                        <h5>{{Lang::get('category.priceFilter')}}</h5>
                                    </div>
                                    <!-- .widget title -->

                                    <!-- widget box -->
                                    <div class="widget-box">
                                        <!-- rised bar -->
                                        <div class="slider-content">
                                            <p>
                                                <span>{{Lang::get('category.priceFrom')}}</span>
                                                <span id="begin-value" class="slide-value">₮0</span>
                                            </p>
                                            <input type="text" class="span2 rised-range-funds" value="" data-slider-min="0" data-slider-max="50000" data-slider-step="10" data-slider-value="0" />

                                        </div>
                                        <!-- .rised bar -->
                                    </div>
                                    <!-- .widget-box -->

                                    <!-- widget box -->
                                    <div class="widget-box">
                                        <a class="button-normal full white" href="#">{{Lang::get('category.searchUpper')}}</a>
                                    </div>
                                    <!-- .widget-box -->

                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- .price filter widget -->

                </div>
                <!-- .sidebar -->

            </div>
        </div>
    </section>
    <!-- .blog posts & widgets -->
    
@stop