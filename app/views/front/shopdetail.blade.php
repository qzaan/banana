@extends('front._layout.layout')
@section('content')

    <div id="pageLoad"></div>

    <!-- breadcrumbs -->
    <section class="breadcrumbs">
        <div class="breadcrumbs-wrapper">
            <div class="container">
                <div class="row">

                    <!-- page title -->
                    <div class="col-md-6 col-xs-6">
                        <h4>{{Lang::get('category.shop')}}</h4>
                    </div>
                    <!-- .page title -->

                    <!-- breadcumbs -->
                    <div class="col-md-6 col-xs-6">
                        <ol class="breadcrumb">
                            <li><a href="/">{{Lang::get('category.home')}}</a>
                            </li>
                            <li><a href="/s">{{Lang::get('category.shop')}}</a>
                            </li>
                            <li class="active"><a href="#">-</a>
                            </li>
                        </ol>
                    </div>
                    <!-- .breadcrumbs -->
                </div>
            </div>
        </div>
    </section>
    <!-- .breadcrumbs -->

    <!-- blog posts & widgets -->
    <section class="no-bottom-margin">
        <div class="container">
            <div class="row">

                <!-- paginate & posts -->
                <div class="col-md-8">

                    <!-- post -->
                    <div class="posts">
                        <div class="row">
                            <div class="col-md-12">


                                <div class="box-wrapper">
                                    <div class="box">
                                        <div class="row">

                                            <!-- slider product images -->
                                            <div class="col-md-6">
                                                <div id="singleCarousel" class="carousel slide single-page normal" data-ride="carousel" data-interval="false">


                                                    <!-- Indicators -->
                                                    <ol class="carousel-indicators shop">
                                                        <?php $i = 0; ?>
                                                        @foreach( $product->productImages as $productImages )
                                                        @if($i == 0)
                                                        <li data-target="#singleCarousel" data-slide-to="{{$i}}" class="active"></li>
                                                        @else
                                                        <li data-target="#singleCarousel" data-slide-to="{{$i}}"></li>
                                                        @endif
                                                        <?php $i++; ?>
                                                        @endforeach
                                                    </ol>

                                                    <!-- Wrapper for slides -->
                                                    <div class="carousel-inner">
                                                        <?php $i = 0; ?>
                                                        @foreach( $product->productImages as $productImages )
                                                        @if($i == 0)
                                                            <div class="item active">
                                                                <img id="image" src="http://lorempixel.com/344/492/" alt="" title="" />
                                                        @else
                                                            <div class="item">
                                                                <img src="{{$productImages->image['path']}}" alt="" title="" />
                                                        @endif
                                                        </div>
                                                        <?php $i++; ?>
                                                        @endforeach
                                                    </div>
                                                    <!-- .carousel-inner -->

                                                </div>
                                                <!-- carousel -->
                                            </div>
                                            <!-- .slider product images -->

                                            <!-- description -->
                                            <div class="col-md-6">
                                                <div class="content comments">
                                                    <div class="clearfix">
                                                        <a class="light-grey pull-left">{{$product->productCategory->title}}</a>
                                                        <span class="light-grey pull-right">
                                                            <span id="quantity0" class="changeColor picked">#</span>
                                                            @foreach( $product->productSize as $productSize )
                                                                <span id="quantity{{$productSize->id}}" class="hidden changeColor">{{$productSize->quantity}}</span>
                                                            @endforeach
                                                            <span class="changeColor">{{Lang::get('category.inStock')}}</span>
                                                        </span>
                                                    </div>
                                                    <h4 id="proTitle">{{$product->title}}</h4>
                                                    <h4 id="proPrice" class="grey">₮<?php echo number_format($product->price) ?></h4>
                                                    <hr />
                                                    <p>
                                                        <?php
                                                            $buffer_desc = $product->description;
                                                            $buffer_desc = (strlen($buffer_desc) >= 150) ? mb_substr($buffer_desc,0,150).'...' : $buffer_desc;
                                                            $buffer_desc_first = mb_substr($buffer_desc,0,1);
                                                            $buffer_desc_other = mb_substr($buffer_desc,1,strlen($buffer_desc));

                                                            $buffer_desc_first = mb_strtoupper($buffer_desc_first);
                                                            $buffer_desc_other = mb_strtolower($buffer_desc_other);

                                                            $buffer_desc_changed = $buffer_desc_first . $buffer_desc_other;
                                                        ?>
                                                        {{$buffer_desc_changed}}
                                                    </p>
                                                    <hr />
                                                    <!-- widget box -->
                                                    <div class="widget-box dropdown">
                                                        <div class="widget-dropdown">

                                                            <a class="button-normal full blue left-text" href="#" data-toggle="dropdown">
                                                                <span id="pick">{{Lang::get('category.pickSize')}}</span>
                                                                <i class="fa fa-angle-down pull-right"></i> 
                                                            </a>
                                                            
                                                            <ul class="dropdown-menu" role="menu">
                                                                @foreach( $product->productSize as $productSize )
                                                                    <li>
                                                                        <a onclick="changePick({{ $productSize->id }},'{{ $productSize->size }}')" href="javascript:void(0)">{{ $productSize->size }}</a>
                                                                    </li>
                                                                @endforeach
                                                            </ul>
                                                        </div>
                                                    </div>
                                                    <!-- .widget-box -->

                                                    <!-- widget box -->
                                                    <div class="widget-box">
                                                        <div class="input-group increment">
                                                            <a class="input-group-addon minus" href="javascript:void(0)">
                                                                <i class="fa fa-minus"></i>
                                                            </a>
                                                            <input type="text" class="form-control" value="0" readonly>
                                                            <a class="input-group-addon plus" href="javascript:void(0)">
                                                                <i class="fa fa-plus"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <!-- .widget box -->

                                                    <!-- widget box -->
                                                    <div class="widget-box">
                                                        <input id="proId" type="hidden" value="{{$product->id}}" />
                                                        <a onclick="pushCart()" class="button-normal full white" href="javascript:void(0)">{{Lang::get('category.addToCartUpper')}}</a>
                                                    </div>
                                                    <!-- .widget box -->

                                                </div>
                                            </div>
                                            <!-- .description -->

                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <!-- .post -->

                    <!-- comments -->
                    <div class="posts">
                        <div class="row">
                            <div class="col-md-12">


                                <div class="box-wrapper">
                                    <div class="box">


                                        <!-- content -->
                                        <div class="content comments clearfix">

                                            <!-- title -->
                                            <h5 class="pull-left">{{Lang::get('category.productDescription')}}</h5>
                                            <!-- .title -->

                                        </div>
                                        <!-- content -->

                                        <hr class="inline-hr" />

                                        <!-- 1 -->
                                        <div class="content comments">
                                            <p>
                                                {{$product->description}}
                                            </p>
                                        </div>
                                        <!-- .1 -->

                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <!-- .comments -->


                </div>
                <!-- .paginate & posts -->

                <!-- sidebar -->
                <div class="col-md-4">

                    <!-- price filter widget -->
                    <div class="widget">
                        <div class="widget-search-causes">
                            <div class="box-wrapper">
                                <div class="box">

                                    <!-- widget title -->
                                    <div class="widget-title">
                                        <h5>{{Lang::get('category.cart')}}</h5>
                                    </div>
                                    <!-- .widget title -->

                                    <div id="myCart"></div>
                                    
                                    <!-- widget box -->
                                    <div class="widget-box total">
                                        <div class="sub-total">
                                            <p class="text-right">{{Lang::get('category.total')}}:
                                                <span id="pro_price">₮0</span>
                                            </p>
                                        </div>
                                    </div>
                                    <!-- .widget box -->

                                    <!-- widget box -->
                                    <div class="widget-box">
                                        <!-- <a class="button-normal white" href="#">{{Lang::get('category.viewCart')}}</a> -->
                                        <a class="button-normal white" href="#">{{Lang::get('category.checkOut')}}</a>
                                    </div>
                                    <!-- .widget-box -->

                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- .price filter widget -->
                </div>
                <!-- .sidebar -->

            </div>
        </div>
        <!-- .blog posts & widgets -->

    </section>

    <!-- similar posts -->
    <section class="no-top-margin">
        <div class="container">
            <div class="row">
                <div class="col-md-12">

                    <!-- heading -->
                    <div class="heading">

                        <!-- title -->
                        <h3>{{Lang::get('category.similarProducts')}}</h3>
                        <!-- .title -->

                        <div class="border">
                            <div class="border-inner">
                            </div>
                        </div>
                        <!-- .carousel slide -->

                    </div>
                    <!-- .heading -->

                </div>
            </div>

            <!-- posts -->
            <div class="posts">
                <div class="row">

                    @foreach($similarProducts as $similarProduct)
                    <div class="col-md-4">
                        <div class="box-wrapper">
                            <div class="box">
                                <div class="latest-box">

                                    <!-- content -->
                                    <div class="content-wrapper">
                                        <div class="content">

                                            <!-- cause meta -->
                                            <div class="meta clearfix">
                                                <a class="pull-left">₮<?php echo number_format($similarProduct->price) ?></a>
                                                <!-- <a class="pull-right cart" data-rel="tooltip" title="{{Lang::get('category.addToCart')}}" href="#"><i class="fa fa-shopping-cart"></i></a> -->
                                            </div>
                                            <!-- .cause meta -->

                                            <!-- excerpt -->
                                            <div class="excerpt">
                                                <h6>
                                                    <?php
                                                        $buffer_title = $similarProduct->title;
                                                        $buffer_title = (strlen($buffer_title) >= 32) ? mb_substr($buffer_title,0,32).'...' : $buffer_title;
                                                        $buffer_title_first = mb_substr($buffer_title,0,1);
                                                        $buffer_title_other = mb_substr($buffer_title,1,strlen($buffer_title));

                                                        $buffer_title_first = mb_strtoupper($buffer_title_first);
                                                        $buffer_title_other = mb_strtolower($buffer_title_other);

                                                        $buffer_title_changed = $buffer_title_first . $buffer_title_other;
                                                    ?>
                                                    <a href="/s/{{$similarProduct->id}}">{{$buffer_title_changed}}</a>
                                                </h6>
                                            </div>
                                            <!-- .excerpt -->

                                        </div>
                                    </div>
                                    <!-- .content -->

                                    <!-- .media -->
                                    <div class="media">

                                        <!-- image -->
                                        <a href="/s/{{$similarProduct->id}}" class="shop">
                                            <div class="sale">{{Lang::get('category.sale')}}</div>
                                            <img src="{{$similarProduct->image}}" class="img-responsive" title="" alt="" />
                                        </a>
                                        <!-- image -->

                                    </div>
                                    <!-- .media -->

                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach

                </div>
            </div>
            <!-- .posts -->

        </div>
    </section>
    <!-- similar posts -->
    
@stop