var headID = document.getElementsByTagName("head")[0];         
var cssNode = document.createElement('link');
cssNode.type = 'text/css';
cssNode.rel = 'stylesheet';
cssNode.href = 'SWITCHER/style.css';
cssNode.media = 'screen';
headID.appendChild(cssNode);

jQuery(document).ready(function ($) {
"use strict";

function update_style( reset ){
	var layout = $('#switcher .layout-select').val();
	var borders = $('#switcher .borders').val();
	var box_style = $('#switcher .box_style').val();
	var mycolor = $('#switcher #mycolor').val();
	var bg_color = $('#switcher #bg_color').val();
	var style_bg = $('#switcher .style_bg').val();
	
	if( layout == 'boxed' ){
		$('.main-wrap').css( 'max-width', '1280px' );
	}
	else{
		$('.main-wrap').css( 'max-width', '100%' );
	}
	
	if( style_bg == 'solid' ){
		$('body').css( 'background', bg_color );
	}
	else{
		$('body').css( 'background', 'url( http://subtlepatterns.com/patterns/'+style_bg+'.png )' );
	}
	
	var switcher_data = {
		layout: layout,
		borders: borders,
		box_style: box_style,
		color: mycolor,
		bg_color: bg_color,
		style_bg: style_bg
	};
	$.ajax({
		url:"SWITCHER/php/dynamic_style.php",
		method: "POST",
		data: switcher_data,
		success:function(data){
			$('#swithcer_style').remove();
			$("head").append("<style id='swithcer_style'>" + data + "</style>");
			$.cookie( 'gorise_data', JSON.stringify(switcher_data) );
			$('.preloader').fadeOut();
		}
	});
}

$('body').append('<!-- theme-switcher -->  <div ID="switcher">     <!-- trigger -->     <div class="trigger">         <span class="fa fa-cog">     </span>       </div>     <!-- .trigger -->     <!-- ===================================================================================================================================== L A Y O U T  S T Y L E ====================================================================================================================================== -->     <!-- LAYOUT -->     <section class="layout">         <!-- title -->         <div class="title">             <h3>         Layout style       </h3>           </div>         <!-- .title -->      <!-- LAYOUT-STYLE -->         <div class="layout-style left">             <!-- caption -->             <div class="caption">                 <p>           Background Style         </p>               </div>             <!-- .caption -->             <!-- dropdown -->             <select class="style_bg">                 <option value="solid" selected="selected">           Solid Color         </option>                 <option value="crossword"> 		 Pattern 1         </option> 		         <option value="light_grey"> 		 Pattern 2         </option> 		         <option value="geometry"> 		 Pattern 3         </option> 		         <option value="dimension"> 		 Pattern 4        </option> 		         <option value="hoffman"> 		 Pattern 5         </option>               </select>             <!-- .dropdown -->           </div>         <!-- LAYOUT-STYLE --> 	     <!-- LAYOUT-STYLE -->         <div class="layout-style left">             <!-- caption -->             <div class="caption">                 <p>           Boxed/Full Width         </p>               </div>             <!-- .caption -->             <!-- dropdown -->             <select class="layout-select">                 <option value="full" selected="selected">           Full Width         </option>                 <option value="boxed">           Boxed         </option>               </select>             <!-- .dropdown -->           </div>         <!-- LAYOUT-STYLE -->         <!-- LAYOUT-STYLE -->         <div class="layout-style left">             <!-- caption -->             <div class="caption">                 <p>           Borders         </p>               </div>             <!-- .caption -->             <!-- dropdown -->             <select class="borders">                 <option value="yes" selected="selected">           Yes         </option>                 <option value="no">           No         </option>               </select>             <!-- .dropdown -->           </div>         <!-- LAYOUT-STYLE --> 	     <!-- LAYOUT-STYLE -->         <div class="layout-style left">             <!-- caption -->             <div class="caption">                 <p>           Box Style         </p>               </div>             <!-- .caption -->             <!-- dropdown -->             <select class="box_style">                 <option value="square" selected="selected">           Square         </option>                 <option value="rounded">           Rounded         </option>               </select>             <!-- .dropdown -->           </div>         <!-- LAYOUT-STYLE -->       </section>     <!-- LAYOUT -->     <hr>     <!-- ===================================================================================================================================== B G  S T Y L E ====================================================================================================================================== -->     <!-- BG-STYLE -->     <!-- bg-style -->     <section class="bg-style clearfix">         <!-- title -->         <div class="title">             <h3>         Background color style       </h3>           </div>         <!-- .title -->         <div class="left">             <div class="bg-color">                 <div class="caption">                     <p>             Pick Site Color           </p>                   </div>                 <div class="color-input">                     <input id="mycolor" class="colorPicker" value="" />                   </div>               </div>           </div> 	     <div class="left">             <div class="bg-color">                 <div class="caption">                     <p>             Pick Bg Color           </p>                   </div>                 <div class="color-input">                     <input id="bg_color" class="colorPicker" value="" />                   </div>               </div>           </div>	       </section>     <!-- .bg-style -->     <!-- .BG-STYLE -->     <hr>     <!-- ===================================================================================================================================== F O O T E R ====================================================================================================================================== -->     <section class="footer">         <div class="left">             <div class="reset">                 <button class="btn btn-default btn-custom">           RESET ALL         </button>               </div>           </div>         <div class="purchase">             <a class="btn btn-default btn-custom" href="http://themeforest.net/item/gorising-charity-nonprofit-fundraising-html/8545442?ref=pebas" target="_blank">         PURCHASE       </a>           </div>       </section>   </div>  <!-- .theme-switcher -->');
$.getScript( "SWITCHER/js/jquery-ui.min.js" ).done(function( script, textStatus ) {
	$.getScript( "SWITCHER/js/evol.colorpicker.min.js" ).done(function( script, textStatus ) {
		$.getScript( "SWITCHER/js/jquery.cookie.js" ).done(function( script, textStatus ) {
			var selected = $.cookie('gorise_data');
			if( typeof selected !== 'undefined' ){
				var data = JSON.parse( selected );
				$('#switcher .layout-select').val( data.layout );
				$('#switcher .borders').val( data.borders );
				$('#switcher .box_style').val( data.box_style );
				$('#switcher #mycolor').val( data.color);
				$('#switcher #bg_color').val( data.bg_color);
				$('#switcher .style_bg').val( data.style_bg );
				
				update_style( false );
			}
			else{
				$('.preloader').fadeOut();
			}
			
			$('.trigger').click(function(){
				if($('#switcher').css('left') == '-320px') {
					$('#switcher').animate({ left:'0px'});
				}
				else (
					$('#switcher').animate({ left:'-320px'})
				);
			});
			
			$("#mycolor").colorpicker();
			$("#bg_color").colorpicker();
			
			$(document).on( 'change', '.layout-select, .borders, .box_style, .style_bg', function(e){
				update_style( false );
			});				
			
			$("#mycolor").on("change.color", function(event, color){
				update_style( false );
			});
			
			$("#bg_color").on( "change.color", function(event, color){
				update_style( false );
			} );
			
			
			$('.reset button').click(function(e){
				e.preventDefault();
				$( '#switcher select' ).each(function(){
					$(this).val( $(this).find('option:eq(0)').attr('value') );
				});
				$('#mycolor').colorpicker( "val", '#00acc1' );
				$('#bg_color').colorpicker( "val", '' );
				update_style( true );
			});
			
		});
	});
});
});
