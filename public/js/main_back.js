jQuery(document).ready(function($) {

    // Register Admin
    $('#registerAdmin').click(function(event) {

        event.preventDefault();
        var form1 = $('#formRegisterAdmin');
        var formData = new FormData(form1[0]);

        $.ajax({
            url                : '/postRegisterAdmin',
            type               : 'POST',
            processData        : false,
            contentType        : false,
            data               : formData,
            success:function(data){
                
                console.log(data);
                $("#error").html("");

                if (data.success == false) 
                {
                    $.each(data.errors,function(index, error){

                        $("#error").append('<div class="alert alert-warning">'+error+'</div>');
                        return false;
                    });
                }
                else if(data.success == true)
                {
                    $("#success").append('<div class="alert alert-warning">'+data.data+'</div>');
                }
            },
            error:function(error){ 
                console.log(error); console.log(error.responseText); alert("Error!!!");
            }
        });

    });

    // Register News Image
    $("#image_files").change(function (e) 
    {    
        if(this.disabled){
            alert('Файл оруулахыг дэмжихгүй байна!');
            return;
        }
        
        $("#selectedFiles").html("");

        var F = this.files;
        if(F && F[0]) for(var i=0; i<F.length; i++) readImage( F[i] , "files" );
    });

    $("#image_file").change(function (e) 
    {    
        if(this.disabled){
            alert('Файл оруулахыг дэмжихгүй байна!');
            return;
        }
        
        $("#selectedFile").html("");

        var F = this.files;
        if(F && F[0]) for(var i=0; i<F.length; i++) readImage( F[i] , "file" );
    });

    function readImage(file, type) 
    {
        var reader = new FileReader();
        var image  = new Image();
      
        reader.readAsDataURL(file);
        reader.onload = function(_file)
        {
            image.src    = _file.target.result;
            image.onload = function() 
            {
                var w = this.width,
                    h = this.height,
                    t = file.type,               
                    n = file.name,
                    s = ~~(file.size/1024) +'KB';

                if(type == "files")
                {
                    /* Setting defualt : 816 x 461 */
                        if(w > 2000 && h > 1500){ swal('Файлын хэмжээ томдож байна! ('+w+'x'+h+')','2000x1500 ихгүй зураг оруулна уу'); $('#image_files').val(""); return; }
                    /* .Setting */
                
                    $('#selectedFiles').append("<img width='450px' height='250px' src=\"" + this.src + "\" style='border:4px double black; margin-left:10px; margin-top:10px'>");

                }
                else if(type == "file")
                {
                    /* Setting */
                        // if( 330 < w < 328 || 187 < h < 185){ swal('Файлын хэмжээ буруу байна!','328x185 хэмжээтэй зураг оруулна уу'); return; }
                    /* .Setting */
                
                    $('#homeModal').modal('show');
                    document.getElementById("homeImageResizing").src = this.src;
                    resizeableImage( $('.home-resize-image') , 'back' );
                }
                else if(type == "files_post")
                {
                    if(w > 2000 && h > 1500){ swal('Файлын хэмжээ томдож байна! ('+w+'x'+h+')','810x458 зураг оруулна уу'); $('#image_files_post').val(""); return; }
                    // if(813 < w < 817 && 455 < h < 461){ swal('Файлын хэмжээ томдож байна! ('+w+'x'+h+')','810x458 зураг оруулна уу'); $('#image_files_post').val(""); return; }
                    $('#selectedFiles_post').append("<img width='450px' height='250px' src=\"" + this.src + "\" style='border:4px double black; margin-left:10px; margin-top:10px'>");
                }
                else if(type == "file_post")
                {
                    if(537 > w > 533 && 409 > h > 403){ swal('Файлын хэмжээ буруу байна! ('+w+'x'+h+')','535x406 зураг оруулна уу'); $('#image_file_post').val(""); return; }
                    $('#selectedFile_post').append("<img width='450px' height='250px' src=\"" + this.src + "\" style='border:4px double black; margin-left:10px; margin-top:10px'>");
                }
            };
            image.onerror= function() {
                swal('Файлын төрөл буруу байна: ', file.type);
            };      
        };
    }

    // Register News
    $('#btn_rgstrNews').click(function (e){

        e.preventDefault();
        var form1 = $('#rgstrNews');
        var formData = new FormData(form1[0]);

        $.ajax({
            url                : '/rgstrNews',
            type               : 'POST',
            processData        : false,
            contentType        : false,
            data               : formData,
            success:function(data){
                
                $("#error").html("");

                if (data.success == false) 
                {
                    $.each(data.errors,function(index, error){

                        $("#error").append('<div class="alert alert-warning">'+error+'</div>');
                        return false;
                    });
                }
                else if(data.success == true)
                {
                    swal("Амжилттай нэмэгдлээ");
                    $('#title').val("");
                    $('#content').val("");
                    $('#image_file').val("");
                    $('#image_files').val("");
                    $('#selectedFiles').html("");
                    $('#selectedFile').html("");
                }
            },
            error:function(error){ 
                console.log(error); console.log(error.responseText); alert("Error!!!");
            }
        });
    });

    $('#homeCloseModal').click(function(e){

        $('#image_file').val("");
        $('#image_files').val("");
        $('#resizer1').remove();
        $('#resizer2').remove();
        $('#resizer3').remove();
        $('#resizer4').remove();
    });

    // Register Post
    $('#btn_rgstrPost').click(function (e){

        e.preventDefault();
        var form1 = $('#rgstrPost');
        var formData = new FormData(form1[0]);

        $.ajax({
            url                : '/rgstrPost',
            type               : 'POST',
            processData        : false,
            contentType        : false,
            data               : formData,
            success:function(data){
                
                $("#error").html("");

                if (data.success == false) 
                {
                    $.each(data.errors,function(index, error){

                        $("#error").append('<div class="alert alert-warning">'+error+'</div>');
                        return false;
                    });
                }
                else if(data.success == true)
                {
                    swal("Амжилттай нэмэгдлээ");
                    $('#title_post').val("");
                    $('#content_post').val("");
                    $('#embed_post').html("");
                    $('#rec_post').val("");
                    $('#cur_post').val("");
                    $('#enddate_post').val("");

                    $('#image_file_post').val("");
                    $('#image_files_post').val("");
                    $("#selectedFiles_post").html("");
                    $("#selectedFile_post").html("");
                }
            },
            error:function(error){ 
                console.log(error); console.log(error.responseText); alert("Error!!!");
            }
        });
    });

    $('#category_post').on('change', function (e) {
        
        $('#category_post_id').val( $("option:selected", this)[0].className );
    });

    $('#usermail_post').on('change', function (e) {
        
        $('#user_post_id').val( $("option:selected", this)[0].className );
    });

    // Register Post Image
    $("#image_files_post").change(function (e) 
    {    
        if(this.disabled){
            alert('Файл оруулахыг дэмжихгүй байна!');
            return;
        }
        
        $("#selectedFiles_post").html("");

        var F = this.files;
        if(F && F[0]) for(var i=0; i<F.length; i++) readImage( F[i] , "files_post" );
    });

    $("#image_file_post").change(function (e) 
    {    
        if(this.disabled){
            alert('Файл оруулахыг дэмжихгүй байна!');
            return;
        }
        
        $("#selectedFile_post").html("");

        var F = this.files;
        if(F && F[0]) for(var i=0; i<F.length; i++) readImage( F[i] , "file_post" );
    });

});

// Edit News
function editNews(id)
{
    alert(id);
}

function removeNews(id)
{
    swal({
       title: "Анхаар!",
       text: "Та энэхүү мэдээг устгах гэж байна!",
       type: "warning",
       showCancelButton: true,
       confirmButtonColor: "#DD6B55",
       confirmButtonText: "Устгах!",
       closeOnConfirm: false}, 
    function(){ 
       
       $.ajax({
            url                : '/removeNews/' + id,
            type               : 'POST',
            success:function(data){
                
                location.reload();
            },
            error:function(error){ 
                console.log(error); console.log(error.responseText); alert("Error!!!");
            }
        });

    }); 
}

function editPost(id)
{
    alert(id);
}

function removePost(id)
{
    swal({
       title: "Анхаар!",
       text: "Та энэхүү хандивыг устгах гэж байна!",
       type: "warning",
       showCancelButton: true,
       confirmButtonColor: "#DD6B55",
       confirmButtonText: "Устгах!",
       closeOnConfirm: false}, 
    function(){ 
       
       $.ajax({
            url                : '/removePost/' + id,
            type               : 'POST',
            success:function(data){
                
                location.reload();
            },
            error:function(error){ 
                console.log(error); console.log(error.responseText); alert("Error!!!");
            }
        });

    }); 
}